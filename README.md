# documentation

This website is built using [Docusaurus 2](https://v2.docusaurus.io/), a modern static website generator.

## To update

Run the following:

```
yarn add @docusaurus/core@next @docusaurus/preset-classic@next
```

## To generate PlantUML diagrams

We use PlantUML to generate Visio-type diagrams for the documentation. This can output diagrams in various formats. We use `.svg` format.

Assuming the `plantuml.jar` file is in the user's home directory and the user is in the `./documentation/diagrams/src/` directory we can generate a specific diagram:

```
java -jar ~/plantuml.jar -o ../../static/img/diagrams -tsvg create_access_application.wsd
```

To generate images from all of the files in the directory:

```
java -jar ~/plantuml.jar -o ../../static/img/diagrams -tsvg *.wsd
```

## To build and deploy

```
yarn build
aws s3 sync --cache-control 'max-age=604800' --exclude index.html ./build s3://raytio-documentation-dev
aws s3 sync --cache-control 'no-cache' --delete ./build s3://raytio-documentation-dev
```

```
aws s3 sync --cache-control 'max-age=604800' --exclude index.html ./build s3://raytio-documentation-prod
aws s3 sync --cache-control 'no-cache' --delete ./build s3://raytio-documentation-prod
```

<!-- From Default README -->

## Installation

```
$ yarn
```

## Local Development

```
$ yarn start
```

This command starts a local development server and open up a browser window. Most changes are reflected live without having to restart the server.

## Formatting

To prevent markdown syntax errors from causing unexpected side effects, [husky](https://npm.im/husky) registers a
git hook which calls [prettier](https://prettier.io/) via [lint-staged](https://npm.im/lint-staged) when you run
`git commit`.
