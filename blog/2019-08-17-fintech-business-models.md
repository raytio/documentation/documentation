---
title: Fintech business models
author: Cameron Beattie
author_title: Founder and CEO
tags: [fintech, banking]
---

## Becoming one of the "unbanked"

Banks are making record profits, but new technologies and data provide an opportunity to disrupt the banking sector. It’s already started overseas, and first movers stand to benefit here in New Zealand. Are you ready to be part of the unbanked revolution?

<!--truncate-->

## NZ banks are the world’s most profitable

Banking is very profitable, with the large New Zealand (Australian-owned) banks returning over 18% on equity, amongst the highest in the world. The smaller banks are much less profitable, averaging around 9% return on equity, mostly due to their relatively higher operating expenses. By way of comparison, Fonterra's return on equity is about 3.5%, whilst Spark's is about 24% on 2019 numbers.

## Interest and fees

Should a fintech take on the banks at their own game? The retail banks earn their revenue from two main sources: interest income and fees. Fees make up roughly 15% of net revenue. For example in the quarter ending March 2019, ANZ NZ earned revenue of about $1bn (after interest expense) of which $800m was net interest and fees income of \$170m. What’s the best revenue model for a fintech - interest or fees?

## More revenue, less profit?

Lending is unusual compared to many other businesses - another dollar of revenue doesn't necessarily result in more profit. Actually there is an unknown impact on profit, determined largely by the lender's likelihood of being repaid.

The risk any fintech has in the lending space, is that they end up making it easy for bad customers (that the banks won't lend to) to get credit. So can a fintech increase either the customer's willingness or likelihood of repaying the loan?

## Willing to pay?

Large, faceless, profit-hungry foreign-owned banks are typically not loved by their customers. By forming an emotional connection with their customers, a fintech should be able to improve their customers’ willingness to pay. There are some interesting models internationally through the use of social good or non-profit vehicles. Alternatively some fintechs attempt to use social connections and stigma to pressure their customer base to repay.

## Credit decisioning - time for a change?

How can a fintech improve the likelihood of their customers repaying loans? By make better assessments. Credit decisions have been made in generally the same way for decades: the customer provides a relatively narrow set of data which the lender attempts to verify, supported perhaps by some third party data. Is there an opportunity to change this?

## Awash with data

Two trends have lead to an exponential increase in data points available about any one individual or business. Firstly consumers are willing to share intimate and personal information with large data-gathering organisations. The most obvious examples include Google via Chrome, search and the Android platform, Facebook through its various properties, Apple and Microsoft through Windows, Office and LinkedIn. Why these organisations have made very little progress in the financial services space, unlike their Chinese counterparts, is surprising. The second trend is the migration from on-premise to cloud software. Customers of these services have gifted one of their most valuable assets, their data and intellectual property, to the platform operators. I expect to see at least some of these organisations moving into financial services. Xero is rumoured to working on offering which will no doubt leverage its large business data set.

## More data = better decisions

Any organisation that can combine these two data sets will have a huge advantage over the traditional lenders in being able to significantly improve the predictive capabilities of the credit decisioning function. This would allow that organisation to target specific customers who their vast data set indicates are more likely to repay, leaving the banks with a more expensive (in terms of credit default) set of customers.

## Payment processing is expensive

The other revenue opportunity for a fintech is to grab a slice of the bank’s fee revenue. Domestic payment transactions are processed in a number of ways including intra-bank (where funds move from one account in a bank to another account in the same bank) and inter-bank (where funds move between banks) using the Exchange Settlement Account System in NZ. About $75bn annually of these payments in NZ are electronic card transactions with merchants paying over $450m of fees (which in turn are on-charged to customers typically through higher prices). Approx 45% of these are credit transactions and about 36% of those don't attract any interest - lots of customers use their credit card for rewards purposes rather than as a means of credit.

## More rewards

The banks have done a very poor job in innovating in the payments space with New Zealand lagging behind many markets, particularly in Europe. So there are a number of opportunities for fintechs in payments. Improved rewards is one fairly obvious means given that about \$12bn annually is transacted on credit cards even though the card-holders evidently don't need credit. By reducing credit risk a higher reward could be offered compared to bank-issued cards.

## More convenience

Greater transactional convenience is another way to gain market share. Since banks use a common platform for inter-bank settlement there is no competitive advantage possible using this standard way of moving funds between banks. Some banks offer applications that provide improved convenience for their own customers. An alternative stored-value payment system (such as that operated by WeChat Pay and Alipay) would enable much greater innovation whilst minimising merchant fees. In China these systems account for more than half of all payments with cards and cash accounting for the other half. Although this would be a fairly obvious choice for the large loyalty programmes it seems unlikely in the short term given Air New Zealand's focus on cost reduction and Loyalty New Zealand's focus on selling data to its stakeholders. Perhaps a new entrant will be able to break the dominance of Mastercard and Visa ?

## Who will be your “unbank”?

There are an estimated 1.7 billion "unbanked" individuals globally. Given the banks' relatively poor data sets and poor track record in innovating in payments I suspect that in the future many of us will increasingly come to rely on "Unbanks" for much of our credit and transactional banking requirements.

If you are a fintech startup or are looking to transform your financial services organisation, [contact Raytio](/contact) to find out how we can help:

- improve onboarding processes and offer a better customer experience using our [digital identity solution](https://www.rayt.io/profilecloud)

- make better credit decisions using our broad range of innovative [APIs](https://api-docs.rayt.io)

- increase customer engagement through a next-generation loyalty programme

- get to market faster using our pre-built building blocks for digital information capture, identity verification, document data extraction, bank integrations and more
- use [graph database technology](https://www.rayt.io/graphcloud) to improve data analytics capabilities
