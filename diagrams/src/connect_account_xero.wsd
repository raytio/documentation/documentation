@startuml
!define GCPPuml https://raw.githubusercontent.com/Crashedmind/PlantUML-icons-GCP/master/dist
!include GCPPuml/GCPCommon.puml
!include GCPPuml/AI_and_Machine_Learning/all.puml
!include <awslib/AWSCommon>
!include <awslib/AWSSimplified.puml>
!include <awslib/Compute/all.puml>
!include <awslib/SecurityIdentityAndCompliance/all.puml>
!include <awslib/CustomerEngagement/all.puml>
!include <awslib/Storage/all.puml>
!include <awslib/NetworkingAndContentDelivery/all.puml>
!include <awslib/ManagementAndGovernance/all.puml>
!include <awslib/mobile/all.puml>
!include <awslib/general/all.puml>
!include <awslib/GroupIcons/all.puml>
!include <tupadr3/common>
!include <tupadr3/font-awesome-5/server>
!include <tupadr3/font-awesome-5/database>
!include <tupadr3/font-awesome-5/stripe>

skinparam linetype polyline
'skinparam linetype ortho

!unquoted procedure $PublicSubnet($MySprite, $alias, $description="", $label="", $technology="", $scale=1, $colour="blue", $shape="", $textsize="18")

    skinparam rectangle {
        backgroundColor<<$alias>> e6ffef
        borderColor<<$alias>> e6ffef
        shadowing<<$alias>> true
    }
    rectangle $alias <<$alias>> as "
        <color:3F8624><$VPCSubnetPublic*.4> Public Subnet</color>\n
        <color:$colour><$MySprite></color>\n$label" 

!endprocedure

!unquoted procedure $PrivateSubnet($MySprite, $alias, $description="", $label="", $technology="", $scale=1, $colour="blue", $shape="", $textsize="18")

    skinparam rectangle {
        backgroundColor<<$alias>> e6fbff
        borderColor<<$alias>> e6fbff
        shadowing<<$alias>> true
    }
    
    rectangle $alias <<$alias>> as "
        <color:3B48CC><$VPCSubnetPrivate*.4> Private Subnet</color>\n
        <color:$colour><$MySprite></color>\n$label" 

!endprocedure

Users(User, "User", " ")
Mobile(Mobile, "Raytio Web app", " ")
Mobile(XeroWeb, "Xero Web app", " ")
package "AWS" {
CloudFront(CloudFront, "CDN", " ")
CloudFront(CloudFront1, "CDN", " ")
CertificateManager(Certificate, "Certificate manager", " ")
APIGateway2(APIGateway, "API", " ")
SystemsManagerParameterStore(Parameters, "Parameter store", " ")
$PrivateSubnet($Lambda, Crud, $label="Xero processor", $colour="D86613")
$PublicSubnet($EC2, Bastion1, $label="Bastion",  $colour="D86613")
S3BucketwithObjects(S3, "Web app assets", " ")
S3BucketwithObjects(S31, "Xero details", " ")
}
FA5_SERVER(External, "Xero", database) #YellowGreen

S3 --> CloudFront: Serve content
CloudFront --> Mobile: Serve content
User --> Mobile: Visits
Mobile --> XeroWeb: 01. Authorization request
Mobile --> CloudFront1: 03. Authorization code
CloudFront1 --> APIGateway: 04. Authorization code
APIGateway --> CloudFront1: 07. Request token
Certificate --> CloudFront: TLS certificate
APIGateway --> Crud: 05. Authorization code
XeroWeb --> Mobile: 02. Redirect to Raytio
Parameters --> Crud: System parameters
Crud --> Bastion1: 06. Request token
Bastion1 --> External: 07. Request token
External --> Bastion1: 08. Access token
Bastion1 --> Crud: 09. Access token
Crud --> S31: 10. Store Xero tokens
Crud --> CloudFront1: 11. Access token
CloudFront1 --> Mobile: 12. Access token
Mobile --> CreatePO: 13. Profile object

@enduml