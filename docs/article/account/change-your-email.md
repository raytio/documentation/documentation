---
id: change-your-email
sidebar_label: Change your email
layout: article
title: Change your email
categories: [account-management]
featured: false
popular: false
tags: [account, email]
---

Your email address can only be changed from the [web safe](https://secretsafe.rayt.io).

:::warning

Changing your email address will log you out of all SecretSafe applications.

:::warning %}

1. Log in to the web safe at <https://secretsafe.rayt.io>
2. Click **Settings** in the top navigation bar
3. Locate the **Change Email** section under **My Account**
4. Type in your master password and the email you want to use
5. Click the **Continue** button
6. Check your email inbox for the verification code from SecretSafe and enter it into the textbox
7. Click the **Change Email** button
8. Log back in to confirm that you can log in using the new email
9. Log out and back in to any other SecretSafe applications that you are using. You will eventually be logged out automatically.
