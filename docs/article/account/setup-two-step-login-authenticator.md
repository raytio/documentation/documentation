---
id: setup-two-step-login-authenticator
sidebar_label: Setup two-step login authenticator
layout: article
title: Set up two-step login with an authenticator app
categories: [account-management]
featured: false
popular: false
tags:
  [
    two-step login,
    2fa,
    two factor authentication,
    account,
    google authenticator,
    authy,
    totp,
  ]
---

SecretSafe supports two-step login by using a third-party authenticator app such as [Authy](https://authy.com/), [Google Authenticator](https://support.google.com/accounts/answer/1066447?hl=en), or [FreeOTP](https://freeotp.github.io/).

## Enable Two-step Login with Authenticator App

:::warning

Two-step login can permanently lock you out of your account. It is very important that you write down and keep your [two-step login recovery code](article/account/lost-two-step-device.md) in a safe place in the event that you lose access to your authenticator app.

:::warning %}

1. Log in to the web safe at <https://secretsafe.rayt.io>
2. Click **Settings** in the top navigation bar, then click **Two-step Login** from the side menu.
3. Select the **Manage** button for the **Authenticator** option and then type in your master password to continue.
4. Follow the steps that appear
   - Download an authenticator app (usually on your mobile device). We recommend [Authy](https://authy.com/).
   - Scan the QR code with the app.
   - Enter the verification code from the app.
5. Click the **Enable** button. A green alert will appear at the top stating that two-step login has been enabled.
6. Click the **Close** button and confirm that the **Authenticator** option now shows as **Enabled**.

## Test

1. **IMPORTANT:** Ensure that you have copied down your [two-step login recovery code](article/account/lost-two-step-device.md) in case something goes wrong.
2. Log out of the SecretSafe website.
3. Log back into the SecretSafe website. You should now be prompted with an authenticator two-step login option.
4. Authenticator protection works with all SecretSafe applications (web, mobile, desktop, browser). Log out of and back in to any other SecretSafe applications that you are using to confirm that two-step login via authenticator app is properly working. You will eventually be logged out automatically.
