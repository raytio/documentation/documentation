---
id: setup-two-step-login-duo
sidebar_label: Setup two-step login duo
layout: article
title: Set up two-step login with Duo Security
categories: [account-management, organizations]
featured: false
popular: false
tags: [two-step login, 2fa, two factor authentication, account, duo, sms]
---

SecretSafe has partnered with Duo Security to bring two-factor authentication to SecretSafe logins, complete with [inline self-service enrollment](https://guide.duo.com/enrollment) and [authentication prompt](https://guide.duo.com/prompt) (offering SMS, phone call, U2F security keys, and push notifications with the Duo Mobile app).

## Overview

This article takes you through configuring your SecretSafe account to use Duo two-factor authentication services. You'll sign up for a Duo account, configure SecretSafe to use your new Duo account, and enroll your SecretSafe account and your device for use with Duo's service.

Once you complete this process, Duo Security's two-factor authentication platform protects access to your SecretSafe data by requiring two-step approval when logging in to your SecretSafe. If you are using this Duo integration with your SecretSafe organization, all users in your organization will be required to complete two-factor authentication with Duo when logging into their SecretSafe.

## Create a Duo Security Account

A Duo account is required to use this feature. A Duo account [for up to 10 users](https://duo.com/pricing) can be created for free.

1. If you do not already have one, sign up for a new Duo account at [https://signup.duo.com/](https://signup.duo.com/)
2. Log in to the Duo Admin panel with your Duo account at [https://admin.duosecurity.com/login](https://admin.duosecurity.com/login)
3. In the left menu, navigate to **Applications**, then click the **Protect an Application** button.
4. Find/search for the **SecretSafe** application and click the **Protect this Application** button.
5. Note the **Integration Key**, **Secret Key**, and **API Hostname** details. We will need to reference these later when configuring SecretSafe.
   ![](/img/images/two-step/duo/application-details.png)

## Get the Duo Mobile App

It is recommended to install the free [Duo Mobile](https://duo.com/product/trusted-users/two-factor-authentication/duo-mobile) app if you want to take advantage of quickly logging in with push notifications. This is optional, however, since Duo also supports SMS, phone calls, and U2F security keys.

- iOS: [Download on the App Store](https://itunes.apple.com/us/app/duo-mobile/id422663827?mt=8)
- Android: [Download on Google Play](https://play.google.com/store/apps/details?id=com.duosecurity.duomobile)

## Enable Two-step Login with Duo

:::warning

Two-step login can permanently lock you out of your account. It is very important that you write down and keep your [two-step login recovery code](../../article/account/lost-two-step-device.md) in a safe place in the event that you lose access to your normal two-step login methods.

:::warning %}

1. Log in to the web safe at <https://secretsafe.rayt.io>.
2. Depending on your account type:

- Users: Click **Settings** in the top navigation bar, then click **Two-step Login** from the side menu.
- Organizations: Visit the admin area for your organization. Select **Settings** in the sub-menu and then click **Two-step Login** from the side menu.

1. Select the **Manage** button for the **Duo** option and then type in your master password to continue.
   ![](/img/images/two-step/duo/select.png)
2. Enter the configuration information provided from the Duo Admin **SecretSafe** application that was set up earlier: **Integration Key**, **Secret Key**, and **API Hostname**.
   ![](/img/images/two-step/duo/config.png)
3. Click the **Enable** button. A green alert will appear at the top stating that two-step login has been enabled.
4. Click the **Close** button and confirm that the **Duo** option now shows as **Enabled**.
   ![](/img/images/two-step/duo/enabled.png)

## Enroll and Test

1. **IMPORTANT:** Ensure that you have copied down your [two-step login recovery code](../../article/account/lost-two-step-device.md) in case something goes wrong.
2. Log out of the SecretSafe web safe (or to be safe incase something is misconfigured, just use a new browser tab so that you can keep your currently logged in browser tab session active).
3. Log back into the SecretSafe web safe. You should now be prompted with a Duo two-step login option.
4. Upon your first login using Duo you may be prompted to enroll your SecretSafe account and device(s) with Duo. Complete the Duo enrollment process following the on-screen instructions.
   ![](/img/images/two-step/duo/enroll1.png)
   ![](/img/images/two-step/duo/enroll2.png)
5. After enrolling you can log in with Duo.
   ![](/img/images/two-step/duo/login.png)
6. Duo security protection works with all SecretSafe applications (web, mobile, desktop, browser). Log out of and back in to any other SecretSafe applications that you are using to confirm that two-step login via Duo is properly working. You will eventually be logged out automatically.

   Desktop
   ![](/img/images/two-step/duo/desktop.png)

   Browser extension
   ![](/img/images/two-step/duo/browser.png)

   Mobile
   ![](/img/images/two-step/duo/android.png)

Congratulations! Your SecretSafe account is now protected by two-step login with Duo Security.
