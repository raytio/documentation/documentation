---
id: import-from-1password
sidebar_label: Import from 1password
layout: article
title: Import your data from 1Password
categories: [getting-started]
featured: true
popular: false
tags: [import, 1password]
---

Importing your data from 1Password into SecretSafe is easy. 1Password has two versions of their application: 1Password 4 and 1Password 6 and 7. Depending on which operating system and version of 1Password and you are using, follow the proper steps below.

## Export Your 1Password 1pif Logins

:::note

This set of instructions is only for:

- 1Password 4 users on macOS and Windows (only the .agilekeychain format is supported for Windows users)
- 1Password 6 and 7 users on macOS

These instructions may vary slightly for macOS users.

:::

1. Open the 1Password desktop application on your computer and enter your 1Password master password to unlock your safe.
2. Select the safe that you wish to export. It is not possible to export from **All Vaults**, so you'll need to switch to a specific safe.
3. Navigate to **File** &rarr; **Export** and an Export window will pop up.
4. In the Export window that pops up, select format **1Password Interchange Format (.1pif)** and **All Items**.
5. Click **OK**. You may be prompted to enter your master password again.
6. Select a folder to save your export file to (recommended to use your desktop folder). Click save to export your .1pif data file.

## Export your 1Password 6 &amp; 7 csv logins from Windows

:::note

This set of instructions is only for:

- 1Password 6 and 7 users on Windows

:::

1. Open the 1Password 6/7 desktop application on your computer and enter your 1Password master password to unlock your safe.
2. Select the items you want to export. Select multiple items by holding down the Ctrl key (Command on macOS) when clicking on them. Select all of the items by pressing Ctrl+A (Command+A on macOS) after clicking one of the items in the list.
3. Click the gear icon in the top right corner and then **Export**.
4. Select the type as **Comma-separated values (\*.csv)**.
5. Select a folder to save your export file to (recommended to use your desktop folder). Enter a file name and click Save.

## Import your logins into SecretSafe

1. Go to the [SecretSafe web safe][secretsafe-safe] and log in.
2. Navigate to **Tools** &rarr; **Import Data**.
3. Select **1Password (1pif)** or **1Password 6 and 7 Windows (csv)** as the file format (depending on which path you followed above) and select your data file from the desktop that you created in the last step from above.
4. Click the **Import Data** button.

Congratulations! You have just transferred your data from 1Password into SecretSafe.

[secretsafe-safe]: https://secretsafe.rayt.io
