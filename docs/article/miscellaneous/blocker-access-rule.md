---
id: blocker-access-rule
sidebar_label: Blocker access rule
layout: article
title: uMatrix and NoScript access rules
categories: [miscellaneous]
featured: false
popular: false
tags: [umatrix, firefox]
---

By default, the uMatrix and NoScript extensions may block the SecretSafe Firefox extension from accessing the SecretSafe API servers. Without adding proper rules to whitelist the SecretSafe API servers, logging in and other API operations will fail.

## uMatrix

The following [uMatrix rule](https://github.com/gorhill/uMatrix/wiki/Rules-syntax) is required:

```
dc8ef5f6-eb0d-4c87-9e9f-0cf803f619e8.moz-extension-scheme rayt.io xhr allow
```

:::note

The UUID included in the above rule (`dc8ef5f6-eb0d-4c87-9e9f-0cf803f619e8`) will be different for your installation.
Use the `about:debugging#/runtime/this-firefox` page (navigate from Firefox's address bar) to locate your SecretSafe extension UUID.

:::

## NoScript

Whitelisting the following domain in NoScript is required: `rayt.io`
