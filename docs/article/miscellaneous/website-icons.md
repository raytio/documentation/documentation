---
id: website-icons
sidebar_label: Website icons
layout: article
title: Your privacy when using website icons
categories: [miscellaneous]
featured: false
popular: false
tags: [icons, website icons, privacy]
---

_SecretSafe does not collect any information when you download icons for website logins stored in your SecretSafe._

## The SecretSafe icons server

When SecretSafe displays a login item associated with a website in your SecretSafe it attempts to accompany it with a "website icon". This "website icons" feature allows you to easily identify particular logins in your safe by a recognizable icon. This is usually represented by a logo or brand image of that website. The SecretSafe icons server provides the delivery endpoint for these website icons.

If you are using the "website icons" feature on a device, SecretSafe will issue requests to `secretsafe-dev.rayt.io/icons` for each item of type "Login" in your safe that has a URI that resembles a website (ex. `google.com` or `https://google.com`, but not `google` or `http://localhost`).

## Privacy concerns

Because a request for an icon image contains the hostname of the website stored in your safe, it is important to understand that this feature will "leak" otherwise cryptographically protected information to SecretSafe servers and/or CDN endpoints. An example of a icon request looks like the following:

`https://icons.secretsafe.net/google.com/icon.png`

**The icons server endpoints do not log or collect any information regarding icon image requests.** However, this is something you would have to take our word for since we have no way to demonstrate this publicly other than reviewing our [open source codebase](https://github.com/raytio/secretsafe).

## Disabling website icons

We understand that certain privacy-minded users may not want to use the "website icons" feature. We provide the option to disable website icons on all SecretSafe client applications:

- **Web safe:** Settings &rarr; Options &rarr; Disable Website Icons
- **Browser extension:** Settings &rarr; Options &rarr; Disable Website Icons
- **Mobile app:** Settings &rarr; Options &rarr; Disable Website Icons
- **Desktop app:** Settings &rarr; Options &rarr; Disable Website Icons

When the website icons feature is disabled, SecretSafe will opt to show you a generic, locally accessed icon instead ({% icon fa-globe ( that is the same for all login items stored in your safe.
