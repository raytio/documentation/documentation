---
id: what-is-an-organization
sidebar_label: What is an organization?
layout: article
title: What are organizations?
categories: [organizations]
featured: true
popular: true
tags: []
---

A SecretSafe organization is an entity that relates users together that want to share items. An organization could be a family, team, company, or any other type of group that desires to share items in SecretSafe.

An individual user account can create and/or belong to many different organizations, allowing you to manage your items from a single account.

You can create a new SecretSafe organization from the [web safe](https://secretsafe.rayt.io) or request that an admin of an existing organization send you an invite.
