---
id: can-secretsafe-see-my-passwords
sidebar_label: Can SecretSafe see my passwords?
layout: article
title: Can the SecretSafe team see my passwords?
categories: [security]
featured: true
popular: false
tags: []
---

No.

Since your data is fully encrypted and/or hashed before ever leaving **your** local device, no one from the SecretSafe team can ever see, read, or reverse engineer to get to your real data. SecretSafe servers only store encrypted and hashed data. This is an important step that SecretSafe takes to protect you.

You can read more about how your data is encrypted and transmitted [here](../../article/security/what-encryption-is-used.md).
