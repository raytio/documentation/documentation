---
id: cloud-server-security
sidebar_label: Cloud server security
layout: article
title: How do you keep the cloud servers secure?
categories: [security]
featured: true
popular: false
tags: [cloud, azure]
---

SecretSafe processes and stores all data securely in the [Amazon Web Services cloud](https://en.wikipedia.org/wiki/Amazon_Web_Services) using services that are managed by the team at AWS. Since SecretSafe only uses service offerings provided by AWS, there is no server infrastructure to manage and maintain. All uptime, scalability, and security updates and guarantees are backed by AWS and their cloud infrastructure.
