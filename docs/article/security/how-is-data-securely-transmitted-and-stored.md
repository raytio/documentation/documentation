---
id: how-is-data-securely-transmitted-and-stored
sidebar_label: How is data securely transmitted and stored?
layout: article
title: How is my data securely transmitted and stored on SecretSafe servers?
categories: [security]
featured: true
popular: false
tags: [encryption]
---

SecretSafe takes security very seriously when it comes to handling your sensitive data. Your data is never sent to the SecretSafe cloud servers without first being encrypted on your local device using [AES](https://en.wikipedia.org/wiki/Advanced_Encryption_Standard) 256 bit encryption. You can read more about SecretSafe encryption [here](../../article/security/what-encryption-is-used.md). SecretSafe never stores meaningful data on its servers.

When your devices sync with the SecretSafe cloud servers, a copy of the encrypted data is downloaded and securely stored to your local device. Whenever you use the SecretSafe apps or extensions your data is decrypted only in memory as needed. Data is never stored in its decrypted form on the remote SecretSafe servers or on your local device.

SecretSafe servers are securely hosted and managed in the [Microsoft Azure cloud](https://en.wikipedia.org/wiki/Microsoft_Azure).
