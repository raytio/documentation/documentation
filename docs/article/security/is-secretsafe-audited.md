---
id: is-secretsafe-audited
sidebar_label: Is SecretSafe audited
layout: article
title: Is SecretSafe audited?
categories: [security]
featured: true
popular: false
tags: [audit]
---

Yes.

By making 100% of our source code available under an open source GPLv3 license, our goal is to be as transparent as possible about how SecretSafe works and how it handles your sensitive data. Being open source also allows thousands of developers to quickly identify potential issues and to verify the quality of our solutions. However, we also understand the need for reputable, independent third-party experts to officially audit the SecretSafe codebase.
