---
id: what-happens-if-secretsafe-is-hacked
sidebar_label: What happens if SecretSafe is hacked?
layout: article
title: What happens if SecretSafe gets hacked?
categories: [security]
featured: true
popular: false
tags: [hacked]
---

SecretSafe takes extreme measures to ensure that its websites, application, and cloud servers are secure. Part of this security comes from the fact that [we rely on managed services and do not manage our cloud server infrastructure at all](article/security/cloud-server-security.md).

However, if for some reason SecretSafe were to get hacked and your data was exposed, your information is still protected. This is because SecretSafe uses strong encryption and one-way salted hashing. As long as you use a strong master password, your data is safe no matter who gets hold of it.
