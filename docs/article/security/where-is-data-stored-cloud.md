---
id: where-is-data-stored-cloud
sidebar_label: Where is data sotred in the cloud?
layout: article
title: Where is my data stored in the cloud?
categories: [security]
featured: true
popular: false
tags: [cloud]
---

SecretSafe processes and stores all data securely in the [Amazon Web Services cloud](https://en.wikipedia.org/wiki/Amazon_Web_Services) using services that are managed by the team at Amazon. SecretSafe does not manage any server infrastructure or security directly. All data is backed up multiple times over, again using services provided by AWS.
