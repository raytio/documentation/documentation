---
id: where-is-data-stored-computer
sidebar_label: Where is data stored on my device?
layout: article
title: Where is my data stored on my computer/device?
categories: [security]
featured: true
popular: false
tags: []
---

Your data is also automatically synced to our [cloud servers](article/security/where-is-data-stored-cloud.md). In the event that you need to recover your data due to a device crash, simply reinstall the SecretSafe application and log in and your data will be re-synced.

All sensitive data stored on your computer/device is encrypted. The data can be found in the following locations:

## Desktop

- Windows
  - Standard Installations &amp; Store: `%AppData%\SecretSafe`
  - Portable: `.\secretsafe-appdata`
- macOS
  - Standard Installations: `~/Library/Application Support/SecretSafe`
  - Mac App Store: `~/Library/Containers/com.secretsafe.desktop/Data/Library/Application Support/SecretSafe`
- Linux
  - Standard Installations: `~/.config/SecretSafe`
  - Snap: `~/snap/secretsafe/current/.config/SecretSafe`

:::tip

You can override the storage location for your SecretSafe desktop application data by setting the `SECRETSAFE_APPDATA_DIR` environment variable to an absolute path.

:::

## Browser Extension

- Windows
  - Chrome: `%LocalAppData%\Google\Chrome\User Data\Default\Local Extension Settings\nngceckbapebfimnlniiiahkandclblb`
  - Firefox: `%AppData%\Mozilla\Firefox\Profiles\your_profile\storage\default\moz-extension+++[UUID]^userContextId=[integer]`
  - Opera: `%AppData%\Opera Software\Opera Stable\Local Extension Settings\ccnckbpmaceehanjmeomladnmlffdjgn`
  - Vivaldi: `%LocalAppData%\Vivaldi\User Data\Default\Local Extension Settings\nngceckbapebfimnlniiiahkandclblb`
  - Brave: `%AppData%\brave\Local Extension Settings\nngceckbapebfimnlniiiahkandclblb`
  - Edge: `%LocalAppData%\Packages\Microsoft.MicrosoftEdge_8wekyb3d8bbwe\AC\MicrosoftEdge\Extensions\Storage`
- macOS
  - Chrome: `~/Library/Application Support/Google/Chrome/Default/Local Extension Settings/nngceckbapebfimnlniiiahkandclblb`
  - Firefox: `~/Library/Application Support/Firefox/Profiles/your_profile/storage/default/moz-extension+++[UUID]^userContextID=[integer]`
  - Safari: `~/Library/Safari/Databases`
- Linux
  - Chrome: `~/.config/google-chrome/Default/Local Extension Settings/nngceckbapebfimnlniiiahkandclblb`
  - Firefox: `~/.mozilla/firefox/your_profile/storage/default/moz-extension+++[UUID]^userContextID=[integer]`

:::note

To enhance security, Firefox uses Universally Unique Identifiers (UUIDs) within extension storage folder names. Use the `about:debugging#/runtime/this-firefox` page (navigate from Firefox's address bar) to locate your SecretSafe extension UUID. Replace [UUID] with that UUID. Note also that Firefox allows users to customize where to store their profiles (and thus local SecretSafe extension data). The location specified above is the default.

:::

## Mobile

- iOS: app group for `group.com.raytio.secretsafe`
- Android: `/data/data/com.xraytio.secretsafe/`

## CLI

- Windows: `%AppData%\SecretSafe CLI`
- macOS: `~/Library/Application Support/SecretSafe CLI`
- Linux: `~/.config/SecretSafe CLI`

:::tip

You can override the storage location for your SecretSafe CLI application data by setting the `SECRETSAFECLI_APPDATA_DIR` environment variable to an absolute path.

:::
