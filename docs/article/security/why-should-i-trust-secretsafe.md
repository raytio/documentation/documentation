---
id: why-should-i-trust-secretsafe
sidebar_label: Why should I trust SecretSafe?
layout: article
title: Why should I trust SecretSafe with my passwords?
categories: [security]
featured: true
popular: true
tags: []
---

1. SecretSafe is 100% open source software. All of our source code is hosted on [GitLab](https://gitlab.com/raytio/secretsafe) and is free for anyone to review. Thousands of software developers follow SecretSafe's source code projects (and you can too!).
2. SecretSafe [is audited](article/security/is-secretsafe-audited.md) by reputable third-party security auditing firms as well as independent security researchers.
3. SecretSafe does not store your passwords. SecretSafe stores encrypted versions of your passwords [that only you can unlock](article/security/can-secretsafe-see-my-passwords.md).
   Your sensitive information is encrypted locally on your personal device before ever being sent to our cloud servers.
4. SecretSafe has a reputation. SecretSafe is used by millions of individuals and businesses. If we did anything questionable or risky we would be out of business.
