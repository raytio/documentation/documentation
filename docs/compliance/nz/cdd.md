---
id: nz-cdd
sidebar_label: Customer due diligence
layout: article
title: Customer due diligence
categories: [CDD]
featured: false
popular: false
tags: [KYC, AML]
---

## AML/CFT Act

The Anti-Money Laundering and Countering Financing of Terrorism Act 2009 describes the requirements of "Reporting Entities" to:

1. appoint an AML/CFT compliance officer (compliance officer)
2. conduct a risk assessment to identify and determine the money laundering (ML) and terrorism financing (TF) risks the entity may encounter in the course of its business
3. develop and implement a programme containing the procedures, policies and controls used to manage and mitigate those risks.

### Reporting Entities are supervised

Trust and Company Service Providers, Financial Institutions and Casinos, Lawyers and Conveyancers, Accountants, Real Estate Agents, High Value Dealers, The Racing Industry and Virtual Asset Providers are supervised by the Department of Internal Affairs which provides a list of reporting entities on the [AML Online website](https://aml.dia.govt.nz/AMLReportingEntities/).

Examples of Trust and Company Service Providers include creation of New Zealand foreign trusts and limited partnerships for international clients, establishment of non-publicly traded companies, limited liability companies (LLCs), and trusts that have no physical presence, running virtual offices.

Examples of services provided by Financial Institutions include cash transport, currency exchange, debt collection, factoring, financial leasing, money remittance, non-bank credit cards, non-bank non-deposit taking lending, payroll, safe deposit boxes, stored value cards, tax pooling.

Banks, life insurers and non-bank deposit takers are supervised by the Reserve Bank of New Zealand and can access information at the [Reserve Bank website](https://www.rbnz.govt.nz). ​

Issuers of securities, trustee companies, futures dealers, collective investment schemes, brokers, and financial advisers are supervised by the Financial Markets Authority and are listed on the [Financial Markets Authority website](https://www.fma.govt.nz/compliance/amlcft/reporting-entities/). In addition, businesses that have formed a "Designated Business Group" are listed on the [FMA website](https://www.fma.govt.nz/compliance/amlcft/designated-business-groups/).

### Customer Due Diligence

The FMA's [AML/CFT Programme Guideline](https://www.fma.govt.nz/assets/Guidance/aml-cft-programme-guideline.pdf) describes the 3 levels of customer due diligence (CDD):

1. Standard CDD - most common. Collection of identity information of the customer, any beneficial owner of the customer or any person acting on behalf of the customer
2. Simplified CDD - specified set of organisations such as Government departments, local authorities, the New Zealand Police and certain listed companies
3. Enhanced CDD - Standard + source of funds/wealth

## [Identity Verification Code of Practice 2013](https://www.fma.govt.nz/assets/Guidance/AML-CFT-identity-verification-code-of-practice-2013.pdf)

### Introduction

The Code applies to reporting entities unless they meet notification requirements and adopt “equally effective” procedures. Most reporting entities have adopted the Code. The Code suggest a best practice for all reporting entities conducting name and date of birth identity verification on customers who are natural persons and who have been assessed to be low to medium risk for AML/CFT purposes.

### Identification and verification

> Identification involves obtaining from the customer a range of information about him or her (“identity information”). Verification involves confirming some of that information against documents, data or information obtained from a reliable and independent source.

### Same information not to be reused

> Reporting entities must check the person’s details against their customer records, to ensure that no other person has presented the same identity information or documents.

### Two methods of identity verification

> This code of practice provides for two ways of conducting identity verification, via documentary verification and electronic verification (Parts 1 and 3 of the code).

### Address verification

> The AML/CFT Act also requires that reporting entities conduct verification of a customer’s address using documents, data or information issued by a reliable and independent source. This code of practice does not prescribe the way in which reporting entities can fulfil this obligation.

### Not for high risk customers

> This code of practice does not apply to the identity verification of customers (that are natural persons) assessed by reporting entities to be high risk. Increased or more sophisticated measures should be applied for high risk customers.

## Documentary verification (Part 1)

### Acceptable documents

The Code prescribes the acceptable forms of documentary verification:

1. One form of the following primary photographic identification: e.g. passport, firearms licence, but NOT DL
1. One form of the following primary non-photographic identification plus a secondary or supporting form of photographic identification: e.g. birth certificate plus DL, 18+ card
1. The New Zealand driver licence and confirmation of details on DL register or other register e.g. passport, citizenship etc

## Document certification (Part 2)

Useful guidance has been [issued by the Law Society](https://www.lawsociety.org.nz/practice-resources/practice-briefings/Certification-under-the-Anti-Money-Laundering-and-Countering-Financing-of-Terrorism-Act-2009.pdf) on the process of certifying a document.

### Reporting entity can rely on a trusted referee

The Code allows reporting entities to verify information by either sighting and retaining a copy of the original identification document, or relying on electronic identification verification, or by relying on a “trusted referee” to sight the original identification document and provide the customer with a certified copy containing approved wording for the retention of the reporting entity.

### Trusted referee must sight the original document

When a reporting entity relies on a certified document to satisfy its verification requirements, it is relying on a trusted referee to sight the original document and to confirm, through the use of appropriate wording, that the quality of the certified document is such that the reporting entity may rely on it.

The trusted referee’s role is to check that the identity of the customer matches the identity of the person named in those documents.

### Verifying vs certifying

Where the referee's firm or organisation is dealing with its own clients, the referee will be ‘verifying’ the identity documentation.

Where the identity documentation relates to a client of another reporting entity, the referee will be ‘certifying’ the identity documentation for the purposes of the reporting entities own verification protocols.

### Photographic identification

Examples include a request to certify or verify a copy of the customer’s passport, driver’s licence, New Zealand firearms licence or an overseas national identity card. Where a customer presents a reporting entity with a duly certified copy of photographic identification, no further documentation is required to attest to the customer’s name and date of birth.

Suggested wording when certifying documentation for customers of other reporting entities is: “I certify this to be a true copy of the original, which I have sighted, and the photo represents a true likeness of [the person presenting the document to me for certification][or] [customer’s name].”

Suggested wording when verifying documentation for lawyers own clients is: “I verify this to be a true copy of the original, which I have sighted, and the photo represents a true likeness of [the person presenting the document to me for verification][or] [customer’s name].”

### Non-photographic identification

If a customer asks a referee to certify/verify a copy of non-photographic identification, the referee needs to consider to what extent he or she is happy to certify/verify that the document “represents the identity of the named individual” – particularly if he or she does not know the customer.

Suggested wording when certifying documentation for customers of other reporting entities is: “I certify this to be a true copy of the original, which I have sighted, and it represents the identity of [the person presenting the document to me for certification][or] [customer’s name].”

Suggested wording where a referee is verifying non-photographic identification for his or her own client: “I verify this to be a true copy of the original, which I have sighted, and it represents the identity of [the person presenting the document to me for verification][or] [client’s name].”

### Non-photographic identification

Because the trusted referee must check identity, it follows that the referee can only certify/verify a document under the Code if it is being **provided by** the person named in that document or if they know that person in a personal capacity

## Electronic verification (Part 3)

The [Identity Verification Code of Practice – Explanatory Note 12/17](https://www.fma.govt.nz/assets/Guidance/Identity-Verification-Code-of-Practice-Explanatory-Note-updated-in-December-2017.pdf)

> provides further clarification to reporting entities that seek to comply with Part 3 of the code by using electronic identity verification.

### Confirm identity AND match the person to the identity

> Electronic verification has two key components, firstly confirmation of identity information via an electronic source(s) and secondly matching the person you are dealing with to the identity that they are claiming (i.e. are they the same person)? Both components must be satisfied.

### Single electronic source

> The code reflects that a reporting entity can satisfy electronic identity verification requirements from a single electronic source that is able to verify an individual’s identity to a high level of confidence. Only an electronic source that incorporates biometric information or information which provides a level of confidence equal to biometric information enables an individual’s identity to be verified to a high level of confidence.

This is probably only a RealMe verified passport.

### Multiple electronic sources

It will therefore be more common that an entity relies on multiple electronic sources for its identity verification.

### Name and date of birth verification

The reporting entity must:

1. verify the customer’s name from either:
   1. a single independent electronic source that is able to verify an individual’s identity to a high level of confidence; or
   2. at least two independent and reliable matching electronic sources.
2. verify the customer’s date of birth from at least one reliable and independent electronic source

### Link source to the person

The explanatory note states that the:

> reporting entity must still have regard to whether the electronic sources include a mechanism to determine if the customer can be linked to the claimed identity

and must:

> consider whether the electronic source(s) has incorporated a mechanism to determine whether the customer can be linked to their claimed identity.

### Additional measures

If the electronic source(s) does not have such a mechanism, or it is not robust enough, then a reporting entity is able to adopt additional measures which could include:

1. Require the first credit into the customer’s account or facility to be received from an account/facility held at another New Zealand reporting entity in the customer’s name.
2. Issue a letter that contains a unique reference/identifier to the customer’s address that has been verified by a reliable and independent source. The letter/unique reference number must be returned to the reporting entity before the customer’s account or facility is fully operational
3. Robust steps to confirm the authenticity of any identification document electronically provided by the customer. This should ensure that both the document belongs to the customer and that it has not been forged, altered or tamperedwith in any waye .g. the original photo on the identification document is replaced.
4. Phone the customer on a number that has been verified by a reliable and independent sourcebeforethe customer’s account or facility is fully operational
5. Robust security type questions based on reliable and independent information obtained about a person’s social or financial footprint. This information should not be publicly available or easily obtained.

### No re-use

Reporting entities must check the person’s details against their customer records, to ensure that no other person has presented the same identity information or documents.

### Beneficial ownership

Reporting entities need to verify the identity of any beneficial owners of the customer, which is [defined by the FMA](https://www.fma.govt.nz/assets/Guidance/121221-beneficial-ownership-guideline.1.pdf) as someone who:

1. owns more than 25 percent of the customer
2. has effective control of the customer
3. is the person on whose behalf a transaction is conducted

A beneficial owner is an individual (a natural person), not a company or organisation.

### Effective control

Effective control of a customer is part of the beneficial ownership definition. Consider:

- those individuals with the ability to control the customer and/or dismiss or appoint those in senior management positions
- those individuals holding more than 25 percent of the customer’s voting rights
- those individuals (for example,the CEO) who hold senior management positions
- trustees (where applicable)

### Person on whose behalf a transaction is conducted

For example if someone (person A) is conducting an occasional transaction on behalf of another person (person B), then person A and person B should be identified and verified along with any other beneficial owners.

### Acting on behalf of a customer

There are instances where persons are acting on behalf of a customer, but are not necessarily beneficial owners of that customer. This is not part of beneficial ownership; it is part of the customer due diligence obligations under the Act.

Acting on behalf of the customer is when a person is authorised to carry out transactions or other activities on behalf of the customer. An example of this is where a customer has one or more individuals who have authority to sign on accounts or authorise the transfer, sale or purchase of assets owned by the customer.

https://www.fma.govt.nz/assets/Guidance/EIV-Explanatory-Note-Guideline-July-2021.pdf

This Explanatory Note and guideline replaces the previous Explanatory Note that was published by the Supervisors in December 2017

EIV is where a customer’s identity is verified remotely or non-face-to-face.

EIV has two key components, both of which must be satisfied:
• Confirmation of identity information via an electronic source(s); and
• Matching the person you are dealing with remotely to the identity that they are
claiming (i.e. are they the same person)?

When using an electronic source that is able to verify an individual’s identity to a
high level of confidence, you are not required to separately link the individual to the
claimed identity

Outsourcing

https://www.dia.govt.nz/diawebsite.nsf/Files/AML-CFT-2021/$file/Outsourcing-CDD-to-a-third-party-provider-Reminder-to-FMA-and-DIA-reporting-entities.pdf

The third-party provider is ... acting as the reporting entity’s agent under section 34 of the AML/CFT Act.

If you outsource the above CDD procedures under section 34 to a third-party provider, the provider must be acting as your agent. In these circumstances, you remain responsible for ensuring that the CDD conducted by the third-party provider is undertaken to the level required by the AML/CFT Act.
