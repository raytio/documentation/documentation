---
id: biometric-checks
sidebar_label: Biometric checks
layout: article
title: Biometric Checks
categories: [features]
featured: false
popular: false
tags: [features, identity, verify, biometrics]
---

## Easy biometric detection

Raytio provides a range of biometric options to meet any customer's needs according to the risk profile, regulatory requirements and desired customer experience. Available biometric checks include:

- Liveness detection
- Liveness detection with audio match
- Selfie and id

## How it works

During the data capture process, the user's camera will be activated on their device. This includes mobile, tablet and computer. Note that there is no need for the customer to install any software or application - all that's required is a web browser.

If the user does not have a camera on their device they can easily transfer across to their camera-enabled device, and continue the process without having to re-enter any data.

The user is then prompted to capture an image or video depending on the tests that have been required by the organisation requesting the data.

To reduce the likelihood of fraud and hacking, the video or image is uploaded to Raytio's proprietary platform. Additional information is passed with the video or image including meta-data, the user's location and device-specifc data. We then perform validation checks on the file using machine learning and heuristics to confirm uniqueness, that there are no fraud or tempering indicators and that all of the necessary liveness and face matching checks have passed.

The image files and test results are then returned to you via our API or attached to the customer’s verification record for you to review and store if desired.

## Liveness detection

The purpose of test is to detect the presence of a human face and that the face moves and performs specific actions.

The user is prompted to record a video of their face. Whilst the video is being recorded, the user's face is detected and they are shown a confirmation. The user is also asked to perform certain actions including opening their mouth and smiling. They are shown a confirmation as the correct action is detected.

## Liveness detection with audio match

The purpose of this test is to detect the presence of a human face, that the face moves and performs specific actions and that the video was not pre-recorded.

Liveness detection with audio match includes the same validations as the liveness detection test with the addition of measures to prevent a pre-recorded video from being used. During recording of the video the user is shown a unique 4 digit code and prompted to say that code out loud.

The video (including the audio) is uploaded to Raytio and speech-to-text analysis is performed to validate that the correct code has been spoken by the user.

## Selfie and id

The purpose of this test is to confirm that the user is the same person as is shown on the identity document and they are not simply using someone else's (possibly stolen) identity document.

The user is prompted to take a photo of their face holding an identity document. Raytio then performs tests on the image of the identity docuement to confirm that it is valid. Additionally the image on the identity document is compared to the selfie of the person and a face match is conducted to verify that the two images match. A probability score is returned which indicates the likelihood that the two face images are of the same person.
