---
id: data-extraction
sidebar_label: Data extraction
layout: article
title: Data extraction
categories: [features]
featured: false
popular: false
tags: [features, form, wizard, OCR, extract]
---

## Introduction to Extra Clarity

Raytio's proprietary data extraction capability recognises text from images either captured from the user's camera or uploaded. We call this "Extra Clarity".

This capability avoids customers having to manually enter data and also allows the capture of document images including not only the entire document, but also parts of the document. For example both the person's photo and the signature can be captured in addition to the entire document. Note that the images are cropped to only the relevant area so the background is eliminated from the scan.

## Clarity models

We have trained a range of Clarity models for various documents and continue to build out our training data set as new requirements arise for our customers.

## Australia

| Document source                                          | Extracted fields                                                                                                                                                                                                                                              | Extracted images                                            |
| -------------------------------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------- |
| Australian (Australian Capital Territory) Driver Licence | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Licence number</li><li>Date of birth</li><li>Date of expiry</li><li>Address</li></ul>                                                                                                       | <ul><li>Document</li><li>Person</li><li>Signature</li></ul> |
| Australian (New South Wales) Driver Licence              | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Licence number</li><li>Date of birth</li><li>Date of expiry</li><li>Address</li></ul>                                                                                                       | <ul><li>Document</li><li>Person</li><li>Signature</li></ul> |
| Australian (Northern Territory) Driver Licence           | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Licence number</li><li>Date of birth</li><li>Date of expiry</li><li>Address</li></ul>                                                                                                       | <ul><li>Document</li><li>Person</li></ul>                   |
| Australian (Queensland) Driver Licence                   | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Licence number</li><li>Date of birth</li><li>Date of issue</li><li>Date of expiry</li></ul>                                                                                                 | <ul><li>Document</li><li>Person</li><li>Signature</li></ul> |
| Australian (Victoria) Driver Licence                     | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Licence number</li><li>Date of birth</li><li>Date of expiry</li><li>Address</li></ul>                                                                                                       | <ul><li>Document</li><li>Person</li><li>Signature</li></ul> |
| Australian Passport                                      | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Passport number</li><li>Passport authority</li><li>Date of birth</li><li>Date of issue</li><li>Date of expiry</li><li>Sex</li><li>Nationality</li><li>Machine readable zone (MRZ)</li></ul> | <ul><li>Document</li><li>Person</li></ul>                   |

## Germany

| Document source | Extracted fields                                                                                                                                                                                                                                                                     | Extracted images                                            |
| --------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------ | ----------------------------------------------------------- |
| German Passport | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Passport number</li><li>Passport authority</li><li>Date of birth</li><li>Date of issue</li><li>Date of expiry</li><li>Sex</li><li>Place of Birth</li><li>Nationality</li><li>Machine readable zone (MRZ)</li></ul> | <ul><li>Document</li><li>Person</li><li>Signature</li></ul> |

## Hong Kong

| Document source         | Extracted fields                                                                                                                    | Extracted images                          |
| ----------------------- | ----------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------- |
| Hong Kong Identity Card | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Card number</li><li>Date of birth</li><li>Date of issue</li></ul> | <ul><li>Document</li><li>Person</li></ul> |

## New Zealand

| Document source                     | Extracted fields                                                                                                                                                                                                                                              | Extracted images                                            |
| ----------------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------- |
| New Zealand Driver Licence          | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Licence number</li><li>Licence version</li><li>Date of birth</li><li>Date of expiry</li></ul>                                                                                               | <ul><li>Document</li><li>Person</li><li>Signature</li></ul> |
| New Zealand Passport                | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Passport number</li><li>Passport authority</li><li>Date of birth</li><li>Date of issue</li><li>Date of expiry</li><li>Sex</li><li>Nationality</li><li>Machine readable zone (MRZ)</li></ul> | <ul><li>Document</li><li>Person</li></ul>                   |
| New Zealand Firearms Licence        | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Licence number</li><li>Date of birth</li><li>Date of expiry</li></ul>                                                                                                                       | <ul><li>Document</li><li>Person</li></ul>                   |
| New Zealand 18 Plus Card            | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Card number</li><li>Date of birth</li><li>Date of expiry</li></ul>                                                                                                                          | <ul><li>Document</li><li>Person</li></ul>                   |
| New Zealand Community Services Card | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Card number</li><li>Client number</li><li>Date of validity</li><li>Date of expiry</li></ul>                                                                                                 | <ul><li>Document</li></ul>                                  |

## Tonga

| Document source       | Extracted fields                                                                                                                                                                                                                                              | Extracted images                          |
| --------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------- |
| Tongan Driver Licence | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Licence number</li><li>Date of birth</li><li>Date of issue</li><li>Date of expiry</li></ul>                                                                                                 | <ul><li>Document</li><li>Person</li></ul> |
| Tongan Passport       | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Passport number</li><li>Passport authority</li><li>Date of birth</li><li>Date of issue</li><li>Date of expiry</li><li>Sex</li><li>Nationality</li><li>Machine readable zone (MRZ)</li></ul> | <ul><li>Document</li><li>Person</li></ul> |

## United Kingdom

| Document source               | Extracted fields                                                                                                                                                                                                                                              | Extracted images                                            |
| ----------------------------- | ------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- | ----------------------------------------------------------- |
| United Kingdom Driver Licence | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Licence number</li><li>Date of birth</li><li>Date of issue</li><li>Date of expiry</li><li>Address</li></ul>                                                                                 | <ul><li>Document</li><li>Person</li><li>Signature</li></ul> |
| United Kingdom Passport       | <ul><li>First name</li><li>Other name</li><li>Family name</li><li>Passport number</li><li>Passport authority</li><li>Date of birth</li><li>Date of issue</li><li>Date of expiry</li><li>Sex</li><li>Nationality</li><li>Machine readable zone (MRZ)</li></ul> | <ul><li>Document</li><li>Person</li></ul>                   |
