---
id: data-verification
sidebar_label: Data verification
layout: article
title: Data verification
categories: [features]
featured: false
popular: false
tags: [features, identity, verify, sources]
---

## Building Trust

Raytio's mission is to build an eco-system of trusted participants. That's why Raytio has the widest range of data verification sources. Of course we verify your customer using traditional data sources such as identity document issuers and credit agencies. We also verify email, phone number, address, bank details and transactions, accounting records, social media profiles, police and court records and more. This provides our customers with the flexibility to choose the most appropriate data sets for their use-case.

## Data sources

[Contact us](https://www.rayt.io/contactus) for a complete list of data sources.
