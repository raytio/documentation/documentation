---
id: form-features
sidebar_label: Form Features
layout: article
title: Form Features
categories: [features]
featured: false
popular: false
tags: [features, form, wizard]
---

## 1. User-Definable Pick Lists

When creating a form, you can refer to custom **Pick Lists** that you create. The pick list is then available to the user  
![a pick list](/img/images/features/pick_lists.png)

## 2. Markdown

You can use **Markdown** to allow for fully personalized forms that support all of markdown's features  
![a markdown page](/img/images/features/markdown.png)

## 3. Survey response style fields

You can create a range of values for users to select to provide an intuitive form for collecting survey-type responses  
![a scale field used for survey responses](/img/images/features/scale.png)

## 4. Tick Boxes

You can create **Tick Boxes** to collect Yes/No or True/False type responses  
![a tick box](/img/images/features/tick_boxes.png)

## 5. Value Ranges

You can create dropdown boxes with **Ranges of Values** to collect numeric data from users  
![a value range dropdown](/img/images/features/value_ranges.png)

## 6. Text Boxes

**Text boxes** can be defined with validation including field minimum and maximum length and regular expressions

## 7. Required Fields

You can set some **Fields to be Required** in a form. These are indicated by a red asterisk and require user input before proceeding or else an error is returned  
![some required fields](/img/images/features/required_fields.png)

## 8. Default Values

You can make some fields automatically set to a **Default Value** to increase usability  
![default values](/img/images/features/default_values.png)

## 9. Address Lookup

You can use the **Address Picker** to auto-fill address information as the user types  
![address picker](/img/images/features/address_picker.gif)

## 10. Conditional Required Fields

You can configure **Conditionally Required** fields that are required based on other form data selected by the user

## 11. Conditional Displayed Fields

You can configure **Conditionally Displayed** fields that display based on other form data selected by the user  
![animated gif of conditional fields](/img/images/features/conditonal_displayed.gif)

## 12. Internationalisation

Forms can be internationalized by you so that users see form field names, titles, help text and pick lists in their own language

## 13. Configurable Display

You can define **Configurable Displays** to configure how data is displayed to the user. You can choose to hide specified fields unless the user chooses to see that information  
![gif of a configurable display being shown and hidden](/img/images/features/configurable_displays.gif)

## 14. Verified Fields

You can configure what fields need to be **Verified**. Raytio automatically verifies the information with authoritative sources. A verified badge is displayed along with indicators next to the verified fields  
![some verified fields](/img/images/features/verified_fields.png)

## 15. Save and Complete Later

You can link a series of forms together to collect complex information from users. As the user steps through each form, their progress is automatically saved. The user can partially complete the forms and then come back later to complete the process (including on another device)  
![list of forms in progress](/img/images/features/list_of_forms_in_progress.png)

## 16. Manage Files

You can upload and manage files using the **File Manager**. Files can be tagged, organized in folders and permissions can be set so that other users (or the public) can access the files  
![gif of a uploading a file to file manager](/img/images/features/file_manager.gif)

## 17. Related Information

You can configure a form to have relationships with other forms so that all **Related Information** can be viewed in one place  
![gif of a some people and their related information](/img/images/features/related_information.gif)
