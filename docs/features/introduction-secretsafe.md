---
id: introduction-secretsafe
sidebar_label: SecretSafe
layout: article
title: SecretSafe Features
categories: [features]
featured: false
popular: false
tags: [features, secretsafe]
---

Raytio SecretSafe is a comprehensive password management solution for individuals, families and organizations:

- Securely store login and password details in the secure [encrypted Vault](../article/security/what-information-is-encrypted)
- Generate secure, random, and unique credentials for every Vault item
- Download encrypted exports for secure storage of Vault [data backups](../article/account/export-your-data)
- [Simple User Management](../article/organizations/managing-users) allows you to easily add or remove seats and onboard or offboard users directly from the Web Vault
- [Enterprise Policies](../article/organizations/policies) enforce security rules for all users, such as mandating use of Two-step Login
- Run reports for Exposed Passwords, Reused Passwords, Weak Passwords, and more
- Run reports for data compromised in known breaches (e.g. Email Addresses, Passwords etc)
- Easily set up [2FA](../article/account/setup-two-step-login)
