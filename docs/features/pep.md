---
id: pep-checks
sidebar_label: PEP & Sanctions
layout: article
title: PEP and Sanction Screening
categories: [features]
featured: false
popular: false
tags:
  [
    features,
    identity,
    verify,
    PEP,
    sanctions,
    watchlist,
    adverse media,
    screening,
  ]
---

## Due diligence made easy

Raytio provides a comprehensive global screening solution that enables customers to comply with regulatory requirements and to reduce the risk of fraud with real-time checks of:

- Politically Exposed Persons (PEP)
- Global Sanctions
- Relatives and Close Associates (RCA)
- Special Interest Person (SIP)
- Special Interest Entities (SIE) and
- Adverse media

## Data sources

Raytio partners with Dow Jones to provide you with easy access to a global database of sanction lists delivering unparalleled coverage of:

- Politically Exposed Persons, their relatives, close associates and the companies they are linked to;
- National and international government sanction lists; and
- Persons officially linked to, or convicted of, high profile crimes or terrorism.

This information is supplemented with news from over 200 countries and more than 500,000 news stories from over 100,000 reputable sources across 40+ languages including non-latin scripts. The data is reviewed daily for relevant content.

Sanctions data is refreshed every eight hours to ensure you’re always up to date with the latest developments.

## Configurable to meet your requirements

Customers can set parameters according to the appropriate risk appetite and reduce false positives. The potential results can be investigated, saved in PDF format for ease of record keeping, and integrated into internal systems if required. Each result is assigned a unique code, allowing you to whitelist false positives when re-screening your customers.

All that is required to perform a check is the individual's first name, surname and date of birth; address and nationality are optional inputs. In seconds, you can view your results through a comprehensive PDF that includes a photo of the individual.
