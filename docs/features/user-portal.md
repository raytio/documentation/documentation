---
id: user-portal
sidebar_label: User Portal
layout: article
title: User Portal
categories: [features]
featured: false
popular: false
tags: [features, identity, portal, web interface, GUI]
---

Raytio's user portal allows organisations to define the information to be collected, request data from users and track completion status, receive notifications of shared data, review data that has been shared, manage the workflow of shared data, manage user access and permissions and receive notifications of data updates.

Raytio's user portal can also be accessed by consumers and used as an ultra-secure place to store and share all personal information such as identity documents, receipts, proof of address documentation and signed agreements.

## Features for consumers (data providers)

- Store all your personal private information securely in one place
- Easily share information with organisations (even if they're not already using Raytio) - no more email, post or visiting offices
- Easily see who has access to your data and get notified when it has been viewed
- Re-use information on your profile to avoid entering the same information repeatedly
- Automatically let others know when your details (e.g. address) change
- Define notification methods and preferences
- Easily store documents such as bank statements, meeting minutes, contracts, receipts etc

## Features for organisations (data receivers)

- Define the information to be collected including identity and address information, documents, signed terms and conditions, survey questions and credit card details (PCI-DSS compliant)
- Request data from users and track completion status
- Define notification methods and preferences
- Receive notifications of shared data
- Where possible, data is verified with authoritative third party data sources including government and banks
- Review the scanned document, the machine-readable extracted data, and the verification details to confirm that the information is legitimate
- Manage the workflow of shared data as it is received, reviewed, accepted or rejected. Communicate with users directly through the user portal
- Receive notifications of data updates
- Manage user access and permissions

## Common features

- All personally identifying information is encrypted client side. That means not even Raytio can see your data.
- Back up encryption keys to ensure continued data access even if you forget your password
- Secure your account with two-feature authentication (2FA)
- Use Raytio on your favourite device - computer, phone or tablet
- No app to install - just use your browser
- Built-in pdf document signing - no software to install
- Complies with all privacy and security requirements including PCI-DSS and GDPR
- Complies will all anti-money laundering regulations
