---
id: audit-trail
sidebar_label: Audit Trail
layout: article
title: Audit Trail
categories: [account-management]
featured: false
popular: false
tags: [account, audit]
---

The Audit trail is a log of all the activity on your Raytio account for transparency. It can be located under **My Account**
![image of the audit trail page](/img/images/raytio/account/audit_trail_page.png)
