---
id: change-password
sidebar_label: Change Password
layout: article
title: Change Password
categories: [account-management]
featured: false
popular: false
tags: [account, password]
---

1. To Change your Raytio password, navigate to the **Account Settings** page under **My Account**. Select **Change Password**.
   ![image of the raytio change password page](/img/images/raytio/account/other_settings.png)

2. Fill out your current password in the box.

   ![image of the current password box filled out](/img/images/raytio/account/current-password.png)

3. Enter your new password, then confirm this password and click **Submit**
   ![image of the raytio confirm change password page](/img/images/raytio/account/confirm_change_password.png)
4. If your password has been changed, you should see a notification on your screen that says **Password Changed Successfully**

   ![image of the raytio password change successful page](/img/images/raytio/account/password_changed_successfully.png)
