---
id: create-an-account
sidebar_label: Create An Account
layout: article
title: Create An Account
categories: [account-management]
featured: false
popular: false
tags: [account, create, signup]
---

:::note

Before you create an account please refer [Personally Identifying Email](../../identityproof/account/personally-identifying-email.md)

:::note %}

1. Go to: [app.rayt.io](https://app.rayt.io/).
2. Press the **SIGN UP** button at the bottom of the screen.
   ![Image of the raytio login page](/img/images/raytio/account/sign_up.png)
3. Enter your email and password into the boxes, then click **Next**.
   ![Image of the sign up screen](/img/images/raytio/account/sign_up_boxes.png)
4. You will then be prompted to enter a verification code
   ![Image of the screen where the verification code is entered](/img/images/raytio/account/enter_verification_code.png)
5. Navigate to your emails, there should be an email from hello@rayt.io with the verification code, copy this into the verification code box and click **Next**.

   ![Image of a verification code email](/img/images/raytio/account/verification_code.png)

6. You will then be redirected to the login page, enter your credentials and click **Login**.
   ![Image of the raytio login page](/img/images/raytio/account/login.png)
7. You will then be taken to the Raytio user home page, Congratulations! you have created your Raytio account
   ![Image of the raytio homepage](/img/images/raytio/account/raytio_homepage.png)
