---
id: export-keys
sidebar_label: Export Keys
layout: article
title: Export Keys
categories: [account-management]
featured: false
popular: false
tags: [account, export, backup]
---

If you forget your password, your encrypted data will no longer be usable by you. To avoid losing all of your data, you can export a backup copy of all your encryption keys now. After resetting your password, import your keys from the backup and your data will still be available to you.

1. Under **Account Settings** select **Security**. You should be presented with a screen which looks like this:
   ![image of the security menu](/img/images/raytio/account/security_menu.png)
2. Press the **Dowload Backup File** button, you will be prompted to enter your password.
   ![image of the confirming password for export key](/img/images/raytio/account/confirm_export_password.png)
3. Enter your password, and click **Authorize**, a download should start in the bottom left of your screen of a file called; raytio-backup-keys.txt.
4. Refer [Import Keys](../../identityproof/account/import-keys.md) to see how to import the key to regain access to your encrypted data.

:::warning

It is important that you **do not** edit this file, otherwise the import process might not work properly, and your encrypted data could be lost

:::warning %}
