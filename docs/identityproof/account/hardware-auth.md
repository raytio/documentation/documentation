---
id: hardware-auth
sidebar_label: Hardware Authentication
layout: article
title: Hardware Authentication
categories: [account-management]
featured: false
popular: false
tags: [account, MFA, biometric, FIDO, passkeys, security]
---

Raytio supports the Web Authentication (WebAuthn) standard to enable a hardware token to be associated with your Raytio account which enables a check to be performed that the account and data is being accessed only by someone without access to that hardware. This also enables biometric authentication using fingerprint or face recognition for example. Other names for this type of authentication include passkeys, FIDO keys or Yubikeys.

Certain types of information (such as identity documents) have been designated by Raytio as requiring authentication with a hardware device when storing the information on your profile. In addition, data receivers may require you to authenticate using the device before data is shared - this confirms to them that only the authorised person is sharing the data and the account has not been compromised. In either of these cases you will be prompted to use your hardware device to authenticate.

Securing your account with a hardware device is a two-step process:

1. registration: this sets up the device on your account and records the id of the device so it is available to be used when required - you can do this at any time. Note that it is possible to register multiple devices on your Raytio account.
2. authentication: when required you will be prompted to authenticate using one of the devices that you have previously registered. Once you authenticate using your hardware device Raytio won't ask you to re-authenticate again for 60 minutes.

The types of device that can be used:

1. FIDO tokens e.g. Yubikey
1. passkeys on Android or Apple devices
1. Windows Hello device

To enable Hardware Authentication on your Raytio account:

1. Navigate to the **Account Settings** page under **My Account** and select **Security**
   ![image of the raytio 2FA page](/img/images/raytio/account/preferences_settings_page.png)
1. In the next window, press the **Enable 2FA** button and a screen like this should appear
   ![image of the raytio 2FA activation page](/img/images/raytio/account/2fa_setup_page.png)
