---
id: import-keys
sidebar_label: Import Keys
layout: article
title: Import Keys
categories: [account-management]
featured: false
popular: false
tags: [account, import, backup]
---

If you recently reset your password, you can regain access to your encrypted data by uploading your key backup file here. This is only possible if you previously exported your raytio-backup-keys.txt file.

1. Navigate to the **Security** section of the **Account Settings** page.
   ![Image of the security menu page](/img/images/raytio/account/security_menu.png)
2. Press the **Restore From Backup** button, and upload the raytio-backup-keys.txt file. It should be located in the downloads folder on your computer. You will be prompted to enter your password, do this and click authorize.
   ![image of the confirming password for export key](/img/images/raytio/account/confirm_import_password.png)
3. You should get a notification at the top of your screen if the import is successful
   ![image of the confirming password for export key](/img/images/raytio/account/import_success.png)
