---
id: personally-identifying-email
sidebar_label: Personally Identifying Email
layout: article
title: Personally Identifying Email
categories: [account-management]
featured: false
popular: false
tags: [account, email, signup]
---

When Creating a Raytio account, it is highly recommended that you use an email that doesn't identify you personally.

A personally identifying email is an email that would allow someone to know your name (or any other personal details) from just looking at your email address e.g John.Smith@gmail.com.

Raytio uses end-to-end encryption, so we can't see your data. However, we can see the email that you have signed up with, so for your safety it is reccommended that you use a non-personally identifying email when creating your Raytio account.
