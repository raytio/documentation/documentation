---
id: settings
sidebar_label: Settings
layout: article
title: Settings
categories: [account-management]
featured: false
popular: false
tags: [account, settings]
---

To make changes to your Raytio account, visit the **Account Settings** page.

1. Navigate to the **Raytio user homepage**.

2. In the top right corner of the page click on **My Account**, then **Account Settings**

3. Within the **Account Settings** menu, you can change your password, inspect your audit trail and update your security preferences
   ![Image of the account settings menu](/img/images/raytio/account/other_settings.png)

4. You can also enable **Dark Mode** and **Encryption**

**Dark mode** toggles the light/dark theme to give Raytio a different look. You can toggle between light/dark mode in the account settings window.

![Image of dark and light mode](/img/images/raytio/account/dark_mode.gif)

By default, all personal data stored on Raytio is encrypted. Enabling **encryption** is highly recommended, however there is an option to disable. Disabling encryption will not affect any existing encrypted data.
