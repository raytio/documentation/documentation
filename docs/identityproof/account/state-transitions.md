---
id: state-transitions
sidebar_label: State Transitions
layout: article
title: State Transitions and Actions
categories: [account-management]
featured: false
popular: false
tags: [account, state, transitions, webhooks, notifications]
---

The State Transitions and Actions table allows definition of the actions that will be taken when the state of application instance changes. For example it is possible to send an email when the state of an instance changes to "Received" (which means that the data receiver has viewed the shared data).

1. Navigate to the **Raytio user homepage**.

2. In the top right corner of the page click on **My Account**, then **Account Settings**
   ![Image of the raytio homepage](/img/images/raytio/account/account_settings.png)
3. Then click on **Other Settings** in the bottom left of your screen
   ![Image of the my account page](/img/images/raytio/account/other_settings.png)
4. Finally, click on the **State Transitions and Actions** box
   ![Image of the other settings page](/img/images/raytio/account/other_settings_page.png)
5. The State Table will then come up on your screen
   ![Image of the state table](/img/images/raytio/account/state_table.png)
6. To add a row, press the **Add A Row** Button (careful this may add it on the next page)
   ![Image of the state table](/img/images/raytio/account/state_table.png)
7. To edit the new row (or any other row), press the blue **Edit** button
   ![Image of the new row](/img/images/raytio/account/new_row.png)
8. Once this is done, make the changes and press **Save** in the bottom
   ![Image of editing a row](/img/images/raytio/account/edit_row.png)
