---
id: two-factor-auth
sidebar_label: Two Factor Authentication
layout: article
title: Two Factor Authentication
categories: [account-management]
featured: false
popular: false
tags: [account, 2FA, security]
---

You can increase the security of your Raytio account by enabling a time-based one-time password (TOTP). This is commonly known as Two Factor Authentication because it combines your Raytio password with another device such as your mobile phone to secure your account. When two factor authentication is activated, you put in your password, then use a code on your mobile device to complete the sign-in.

There are a number of mobile apps that support generation of a TOTP including Google Authenticator and Microsoft Authenticator.

1. To enable Two Factor Authentication on your Raytio account, navigate to the **Account Settings** page under **My Account** and select **Security**
   ![image of the raytio 2FA page](/img/images/raytio/account/preferences_settings_page.png)
2. In the next window, press the **Enable 2FA** button and a screen like this should appear
   ![image of the raytio 2FA activation page](/img/images/raytio/account/2fa_setup_page.png)

## Google Authenticator

1. To enable two factor authentication using the Google Authenticator, first navigate to your mobile devices app store and search Google Authenticator.
   ![image of the google authenticator on the play store](/img/images/raytio/account/google_auth.png)
2. Install the app and open it.
3. Press **Get Started**, and then **Scan A QR Code**, if prompted to allow the use of your camera, press **allow**
4. Aim the camera at the QR code on your screen, you should be redirected to a screen that says Account added, and a code should show up on-screen. Enter this code into the **enter code** box on the Raytio website, and click **Verify Code**.
   ![image of the enter 2FA code screen filled out with code](/img/images/raytio/account/enter_2fa_code.png)
5. There should be a confirmation screen that comes up, and verifies that you have enabled two factor authentication
   ![image of the 2FA activated screen](/img/images/raytio/account/2fa_activated.png)

## Microsoft Authenticator

1. To enable two factor authentication using the Microsoft Authenticator, first navigate to your mobile devices app store and search Microsoft Authenticator.
   ![image of the google authenticator on the play store](/img/images/raytio/account/microsoft_auth.png)
2. Install the app and open it.
3. Press **Scan QR Code**, if prompted to allow the use of your camera, press **allow**
4. Aim the camera at the QR code on your screen, you should be redirected to a screen with the Raytio two factor authentication profile, click on this profile. You should see a code in the middle of the screen. Enter this code into the **enter code** box on the Raytio website, and click **Verify Code**.
   ![image of the enter 2FA code screen filled out with code](/img/images/raytio/account/enter_2fa_code.png)
5. There should be a confirmation screen that comes up, and verifies that you have enabled two factor authentication
   ![image of the 2FA activated screen](/img/images/raytio/account/2fa_activated.png)
