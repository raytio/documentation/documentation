---
id: add-to-profile
sidebar_label: Add Profile Items
layout: article
title: Add Items To Your Profile
categories: [data-provider]
featured: false
popular: false
tags: [data provider, categories, profile items]
---

You can securely store information of various categories with Raytio. These categories are pre-defined by Raytio or may be created by other organizations to capture information that is relevant to them.

1. Navigate to the Raytio homepage, in the top right of your screen, press on the **Data Provider** dropdown, then on **Profile**

   ![image of the data provider dropdown](/img/images/raytio/data-provider/my_profile.png)

2. To add a category to your profile, press the **Add Category** button

   ![image of the my profile homepage](/img/images/raytio/data-provider/my_profile_page.png)

3. On the list that pops up, choose which category you would like to add, and press on the right arrow **>**  
   ![image of the select category type popup](/img/images/raytio/data-provider/add_category_to_profile.png)
4. A form will be shown with fields to complete depending on which category you have selected. Fill out the form with the information requested, and then click **Submit**  
   ![image of the add nationality category form](/img/images/raytio/data-provider/nationality.png)
5. The newly-added category will be selected and the completed form will be shown with the information you just filled out
   ![image of the my profile homepage with the new profile object added](/img/images/raytio/data-provider/created_category.png)
6. When you have added multiple categories, you may switch between them with the dropdown box in the top right corner
   ![image of the multiple categories being selected in the my profile page](/img/images/raytio/data-provider/select_category.png)
7. To edit a profile item, select the category and then find the item that you would like to edit. Press the **Actions** dropdown box and then select **Edit**  
   ![image of the actions dropdown box](/img/images/raytio/data-provider/actions.png)
8. To view the history of a profile item, select the category and then find the item that you would like to view the history of. Press the **Actions** dropdown box and then select **View History**  
   ![image of the actions dropdown box](/img/images/raytio/data-provider/actions.png)

If you would like to update the permission of your profile object, refer **[Update Profile Object Permissions](../../identityproof/data-provider/profile-permissions.md)**
