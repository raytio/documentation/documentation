---
id: file-manager
sidebar_label: File Manager
layout: article
title: File Manager
categories: [data-provider]
featured: false
popular: false
tags: [data provider, files]
---

You can use Raytio to store and share files

1. Navigate to the Raytio homepage, in the top right of your screen, press on the **Data Provider** dropdown, then on **My Files**  
   ![image of the data provider dropdown](/img/images/raytio/data-provider/shared_data.png)
2. To upload a file, press the **Upload File** button  
   ![image of my files homepage](/img/images/raytio/data-provider/my_files_homepage.png)
3. Fill out the form, and upload the file by clicking the **Click to Upload** button, then click **Submit**  
   ![image of the upload file form filled out](/img/images/raytio/data-provider/upload_file.png)
4. You will be redirected back to the **Files** page, with your new file shown  
   ![image of the my files homepage with new file added](/img/images/raytio/data-provider/my_files_homepage_new_file.png)
5. To download a file that you have uploaded to Raytio, on the right side of the file you would like to download, press the **Download** button (the arrow pointing downwards into a box)  
   ![image of the download file button](/img/images/raytio/data-provider/download_file.png)
6. To update file permssions, on the right side of the file you would like to share, press the **Actions** dropdown box (the three horizontal lines) and press **Share**. There are three permissions:
   1. Public - anyone can view the file
   2. All Raytio Users - any authenticated Raytio user can view the file
   3. Specific Users Only - only selected users that you have shared the file with can access it  
      ![image of the share file button](/img/images/raytio/data-provider/share_file.png)
7. To make the chosen category publicly available, press on the **Public** tab, then **Save**
   ![image of the share public page](/img/images/raytio/data-provider/share_file_public.png)
8. To make the chosen category only available to Raytio users, press on the **All Raytio Users** tab, and then **Save**
   ![image of the share with Raytio users only](/img/images/raytio/data-provider/share_file_raytio.png)
9. To make the chosen category only available to specific people from your organization, press on the **Specific Users Only** tab, and then **Add Someone**, select who you would like to share it with, then press **Share**
   ![image of the share file with specific users only](/img/images/raytio/data-provider/share_file_specific.png)
