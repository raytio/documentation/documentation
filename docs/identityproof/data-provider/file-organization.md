---
id: file-organization
sidebar_label: File Directories
layout: article
title: File Directories
categories: [data-provider]
featured: false
popular: false
tags: [provider, files]
---

You can organize files in directories

1.  You can store your files in different directories to help organize them. When creating or editing a file you can enter the name of a directory in the **Directory** field. Create subdirectories by separating the directory names with **/**  
    ![image of the directory box filled out in the edit file form](/img/images/raytio/data-provider/directory_form.png)
2.  Files with directory references are shown separately  
    ![image of the files page with the test file inside two test directories](/img/images/raytio/data-provider/directories.png)
3.  To put multiple files in the same directory, just use the directory name in the **Directory** box when creating the file
