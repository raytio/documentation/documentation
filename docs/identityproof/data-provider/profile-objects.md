---
id: profile-objects
sidebar_label: Profile Objects
layout: article
title: Profile Objects
categories: [account-management]
featured: false
popular: false
tags: [data provider, categories]
---

To share your data with others, you need to add categories to your profile

1. Navigate to the Raytio homepage, in the top right of your screen, press on the **Data Provider** dropdown, then on **Profile**

   ![image of the data provider dropdown](/img/images/raytio/data-provider/my_profile.png)

2. To add a category to your profile, press the **Add Category** button

   ![image of the my profile homepage ](/img/images/raytio/data-provider/my_profile_page.png)

3. On the list that pops up, choose which category you would like to add, and press on the **Arrow**
   ![image of the select category type popup](/img/images/raytio/data-provider/add_category_to_profile.png)
4. Depending on which category you have selected, fill out the box with the information requested, and then click **Submit**
   ![image of the add nationality category form](/img/images/raytio/data-provider/nationality.png)
5. You should see your newly added category selected with the information you just filled out
   ![image of the my profile homepage with the new profile object added](/img/images/raytio/data-provider/created_category.png)
6. When you have added multiple categories, you may switch between them with the dropdown box in the top right corner
   ![image of multiple categories being selected in the my profile page](/img/images/raytio/data-provider/select_category.png)
7. You can edit the categories on your profile, navigate to the category that you would like to edit, press on the **Actions** dropdown box and then edit
   ![image of the actions dropdown box](/img/images/raytio/data-provider/actions.png)
8. You can view the history of the categories on you profile, navigate to the category that you would like to view the history of, press on the **Actions** dropdown box and then **Edit**
   ![image of the actions dropdown box](/img/images/raytio/data-provider/actions.png)

If you would like to update the permission of your profile object, refer **[Update Profile Object Permissions](../../identityproof/data-provider/profile-permissions.md)**
