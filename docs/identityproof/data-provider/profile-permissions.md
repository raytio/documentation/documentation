---
id: profile-permissions
sidebar_label: Profile item permissions
layout: article
title: Set permissions on your profile items
categories: [data-provider]
featured: false
popular: false
tags: [data provider, categories, profile items, permissions]
---

You can set the permissions on any of your profile items.

1. To update profile items permssions, go to the category you wish to share and press the **Actions** dropdown box and press **Share**. There are three permissions:
   1. Public - anyone can view the file
   2. All Raytio Users - any authenticated Raytio user can view the file
   3. Specific Users Only - only selected users that you have shared the file with can access it  
      ![image of the actions dropdown box](/img/images/raytio/data-provider/category_share.png)
2. To make the chosen item publicly available, press on the **Public** tab, and then **Save**
   ![image of the manage sharing popup on the public tab](/img/images/raytio/data-provider/share_public.png)
3. To make the chosen item only available to Raytio users, press on the **All Raytio Users** tab, and then **Save**
   ![image of the manage sharing popup on the all raytio users tab](/img/images/raytio/data-provider/share_raytio.png)
4. To make the chosen item only available to specific people form your organization, press on the **Specific Users Only** tab, and then **Add Someone**, select who you would like to share it with, then press **Share**
   ![image of the manage sharing popup on the specific users only tab](/img/images/raytio/data-provider/share_specific.png)
