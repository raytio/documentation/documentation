---
id: reshare-data
sidebar_label: Reshare Data
layout: article
title: Reshare Data
categories: [data-provider]
featured: false
popular: false
tags: [data provider, files]
---

Re-share allows a user to share items with a second party that have previously been shared with one party

1. Navigate to **Shared Data** underneath the **Sharing** heading from the Raytio homescreen.  
   ![image of the shared data homescreen](/img/images/raytio/data-provider/shared_data_hp.png)
2. Select the items you want to reshare.  
   ![image of the selected items](/img/images/raytio/data-provider/selected_items.png)
3. Choose **Actions**, then **Share Submissions**

   ![image of actions then share sub](/img/images/raytio/data-provider/actions_sharesub.png)

4. Enter the ID of the access application you want to share your data with - this will have been provided to you by the data recipient. Then press **Share**  
   ![image of application ID screen](/img/images/raytio/data-provider/share_sub.png)
5. A confirmation will by displayed confirming that your data has been reshared.
