---
id: share-categories
sidebar_label: Update Permissions
layout: article
title: Update Profile Object Permissions
categories: [account-management]
featured: false
popular: false
tags: [data provider, categories, share]
---

You can share the categories on your profile with others. You can make them public so anyone can view that information, make them only available to Raytio users or only available to select people you have shared it with.

1. To share a category, go to the category you wish to share and press the **Actions** dropdown box, and then **Share**
   ![image of the actions dropdown box](/img/images/raytio/data-provider/category_share.png)
2. To make the chosen category publicly available, press on the **Public** tab, and then **Save**
   ![image of the manage sharing popup on the public tab](/img/images/raytio/data-provider/share_public.png)
3. To make the chosen category only available to Raytio users, press on the **All Raytio Users** tab, and then **Save**
   ![image of the manage sharing popup on the all raytio users tab](/img/images/raytio/data-provider/share_raytio.png)
4. To make the chosen category only available to specific people form your organization, press on the **Specific Users Only** tab, and then **Add Someone**, select who you would like to share it with, then press **Share**
   ![image of the manage sharing popup on the specific users only tab](/img/images/raytio/data-provider/share_specific.png)
