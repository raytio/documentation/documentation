---
id: share-verifications
sidebar_label: Share Verifications
layout: article
title: Share Verifications
categories: [data-provider]
featured: false
popular: false
tags: [data provider, share, verifications]
---

You can share multiple verifications with third parties to prove certain facts without revealing private or sensitive information.

1. Navigate to the verifications page, and press the **Share Verifications** button to bring up the screen with all of your verifications on it  
   ![image of the verifications page](/img/images/raytio/data-provider/share.png)

2. Select the verified facts that you would like to share. Selecting the box next to the verification type will share all of the facts for that verification - you would normally not want to do that. Selecting the smaller ticks will share the information of only the data value selected. Once all of the desired ticks have been selected, scroll down to the bottom, and then press share, and a link will be created  
   ![image of the share multiple verifications screen](/img/images/raytio/data-provider/share_verifications.png)
3. Share this link with the person(s) that you would like to share the verifications with
