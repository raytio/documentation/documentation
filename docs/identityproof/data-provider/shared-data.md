---
id: shared-data
sidebar_label: Shared Data
layout: article
title: Shared Data
categories: [data-provider]
featured: false
popular: false
tags: [data provider, files]
---

You can view (and see the history of) the data that you have shared with others

1. Navigate to the Raytio homepage. In the top left of your screen, press on the **Sharing** dropdown, then on **Shared Data**
2. On the **Shared Data** page the list of data submissions that you made will be displayed  
   ![image of the shared data homepage](/img/images/raytio/data-provider/shared_data_homepage.png)
3. Select the three horizontal lines on the right hand side of your screen under **Actions**. A dropdown menu will appear with three options **Open**, **Review** and **View History**.

   ![image of the actions dropdown menu](/img/images/raytio/data-provider/actions_dropdown_menu.png)

4. Click **Open** to see the data that was shared with the application as well as the date it was shared and its expiry date. In this screen, there will also be an option to print a copy of your submission.  
   ![image of the open application screen](/img/images/raytio/data-provider/open_application_page.png)
5. Click **Review** to give feedback regarding your experience. For each submission, you are able to leave a rating and/or written comments about your interaction with the service provider.  
   ![image of the review screen](/img/images/raytio/data-provider/review_screen.png)
6. Click **View History** to see the history of the submission including when the data was shared, when the data receiver updated the status and when any changes to the data have occurred.

   ![image of the information dropdown from a shared file](/img/images/raytio/data-provider/history1.png)
