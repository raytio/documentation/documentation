---
id: todo
sidebar_label: To Do List
layout: article
title: To Do List
categories: [data-provider]
featured: false
popular: false
tags: [data provider, todo]
---

:::note

The **To Do List** will only appear when you have tasks to do, it will not be there all the time

:::note %}

The To Do List is a list of all the unfinished Data Provider tasks you may have

1. Navigate to the **To Do List** by clicking on the **Tick** in the top right corner of your screen

   ![image of the data provider dropdown](/img/images/raytio/data-provider/todo.png)

2. There should be a page with all of your To Do's on it
   ![image of the data provider dropdown](/img/images/raytio/data-provider/todo_homepage.png)
3. To complete the task, find the task you would like to complete, and on the right side press **Continue**
