---
id: verifications
sidebar_label: Verifications
layout: article
title: Verifications
categories: [data-provider]
featured: false
popular: false
tags: [data provider, verifications]
---

When you add items to your profile, Raytio will verify that information where possible. For example, when you add an identity document, we will attempt to verify that information with the authoritative source (for example the government) and will add that verification to your profile.

:::tip
Each field on a profile item is verified individually so there are multiple verifications for a single profile item. For example an identity card may contain an id number, an expiry date, name, and date of birth. Each of these fields will be verified individually. The verifications page groups all related field verifications into a single verification badge for that profile item
:::tip

1. Navigate to the Raytio homepage, in the top right of your screen, press on the **Data Provider** dropdown, then on **Verifications**  
   ![image of the data provider dropdown](/img/images/raytio/data-provider/verifications.png)
2. A summary of your verifications is shown. In this example verifications are shown for two companies and one email address  
   ![image of the verifications page](/img/images/raytio/data-provider/verifications_page.png)
3. You can set an expiry date for the verification by pressing the **Delete** button (the clock). This sets a date after which the verification will no longer be accessible or usable.

   :::note
   Raytio will automatically set an expiration date on verifications if the verification should only be relied on for a certain period of time.  
   :::note
   ![image of the expire verification page](/img/images/raytio/data-provider/expire_verification.png)

4. You can share the verification with someone else by pressing the **Share** button.
   1. The person that you share the verification with will be able to see the value of any field that you select and whether that value has been successfully verified.
   2. If the field is not selected then the person that views the verification will still be able to view the verification result, but they will not be able to see the data that was verified.  
      ![image of the select which verification fields to share](/img/images/raytio/data-provider/select_share.png)

:::warning
Be careful which data you make available to third parties. Once a verification is shared it cannot be "unshared". The only way to prevent someone having access to the verification is to expire that verification.
:::warning

5. Once you have selected the desired verification fields, press **Share**. A link will be generated which you can shared with anyone that you would like to see your selected verifications
   ![image of the share verifications copy link page](/img/images/raytio/data-provider/share_verification_link.png)
6. The person that you have shared the link with will see the values of the data that you have permitted them to see, as well as the verification result for each verified field
   ![image of what the receiver of the link will see](/img/images/raytio/data-provider/verify_data.png)
7. You can see the details of the verification for each individual entity by pressing the **View Details** button (the two arrows pointing outwards). This will show  
   ![image of the status of the entity's in on the verifications page](/img/images/raytio/data-provider/entity_status.png)
