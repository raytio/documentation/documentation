---
id: application-actions
sidebar_label: AA Actions
layout: article
title: Access Application Actions
categories: [data-receiver]
featured: false
popular: false
tags: [data receiver, submissions, actions, pdf, report]
---

To interact with data which has been shared with you, navigate to the desired access application under **Submissions**. Select the submission(s) that you want to perform further actions on and click the **Actions** button on the right hand side of your screen.  
![gif of the actions dropdown box](/img/images/raytio/data-receiver/actions_menu.gif)

## Share Submission

You can share the data from a submission to another application

1. From the **Actions** dropdown menu, select **Share x submission/s**

2. Enter the **Application ID** of the application you want to share the data with and a **Reference**. Then click **Submit**

3. A confirmation will be displayed that the data has been on-shared  
   ![image of on-share success](/img/images/raytio/data-receiver/onshare_success.png)

## Download Report

You can download an Adobe PDF report of the submitted data.

1. From the **Actions** dropdown menu, select **Download x sumbission report/s**  
   ![image of the downloaded sucessfully screen](/img/images/raytio/data-receiver/dload_yes.png)
2. Click **Download** to open the file or save it to your computer

## Email Report

You can send an Adobe PDF report of the shared data to any email address.

1. From the **Actions** dropdown menu, select **Email x submission report/s**

   ![image of the email report screen](/img/images/raytio/data-receiver/email_report.png)

2. Enter the email/s you would like the data to be sent too and click submit.

   ![image of relevant email entered](/img/images/raytio/data-receiver/enter_email1.png)

3. A confirmation will display that the report is being sent. The recipient will then receive an email that has the **Report.pdf** file attached

   ![image of the success screen](/img/images/raytio/data-receiver/email_success1.png)
   ![image of the email containing the report](/img/images/raytio/data-receiver/email-confirmation.png)
