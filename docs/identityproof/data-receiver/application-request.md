---
id: application-request
sidebar_label: Request
layout: article
title: Application Request
categories: [data-receiver]
featured: false
popular: false
tags: [data receiver, submissions, actions, pdf, report]
---

## You can send out a request via email for a specific application

Navigate to the Application Request page by clicking on the **Notepad** icon in the top right of your screen from the Raytio homepage
![image of the notepad icon](/img/images/raytio/data-receiver/notepad.png)

From the Application Request page, you can view your pending requests, or send a new request via email.

To send a request via email, press the **Email Application** button on the right hand side of your screen.
![image of the application request page](/img/images/raytio/data-receiver/application_request.png)

You will now be taken to a screen with all of your forms. You may toggle on and off if (all forms are shown?). Select the form that you would like to request, and then scroll down to the bottom of the screen and press **Continue**.
![image of the application email link page](/img/images/raytio/data-receiver/application_email_link.png)

From here, enter the email(s) of the people you would like to send the request out to. You may enter a reference that will appear on their statement.

![image of the enter email for application page](/img/images/raytio/data-receiver/enter_application_email.png)

You will then be taken back to the Application Requests homepage where you will see the newly sent request and it's status
![image of the enter email for application page](/img/images/raytio/data-receiver/application_request_sent.png)
