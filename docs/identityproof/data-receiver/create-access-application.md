---
id: create-access-application
sidebar_label: Create Access Application
layout: article
title: Create Access Application
categories: [data-receiver]
featured: false
popular: false
tags: [data receiver, application]
---

An Access Application is used to collect information from data providers. It is a set of `scopes` (or `schema`) that define the information to be collected.

1. To create an access application, first navigate to the Raytio homepage. In the top left corner of your screen press on the **Data Receiver** dropdown box, and then **Applications**

   ![image of the data receiver dropdown box](/img/images/raytio/data-receiver/applications.png)

2. Press the **Create New Access Application** button to begin the creation process
   ![image of the access application page](/img/images/raytio/data-receiver/applications_page.png)
3. Fill out the form with the information of your Access Application:

| Name                                  | Description                                                                                                                                                                                                                                                                                                                  |
| ------------------------------------- | ---------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------------- |
| Access Application Name               | The name of the access application                                                                                                                                                                                                                                                                                           |
| Access Application Description        | The description of the access application                                                                                                                                                                                                                                                                                    |
| Callback URIs                         | The list of valid access application callback URIs. When your application directs the user to our web application, you can specify a callback URI that the data provider will be directed to when they submit the data to be shared. That callback URI must be in the list of valid URIs specified on the access application |
| Logout URI                            | The access application logout URI (not used currently)                                                                                                                                                                                                                                                                       |
| Access Application image              | The picture to show to the user when they are completing the application. Usually this will be your company or product logo                                                                                                                                                                                                  |
| Tags                                  | An array of user-defined tags to allow grouping and searching of access applications                                                                                                                                                                                                                                         |
| Access Application Schema             | The valid `schema` or `scopes` for this access application                                                                                                                                                                                                                                                                   |
| State transitions and actions (Table) | The state transitions and actions to perform for each transition                                                                                                                                                                                                                                                             |
| Custom theme                          | The properties of the custom theme for this access application. You can define a custom look and feel which makes the forms look consistent with your branding                                                                                                                                                               |
| Generate Access application secret    | Whether to generate a secret or not (not used currently)                                                                                                                                                                                                                                                                     |
| Related service provider              | The service provider that owns this access application                                                                                                                                                                                                                                                                       |

4. Once you have filled out the form, click **Submit**

   ![image of the access application for filled out](/img/images/raytio/data-receiver/create_access_application.png)

5. You should be taken to a page that shows all of your access applications, including the one you just created
   ![image of the access application page with created access application](/img/images/raytio/data-receiver/access_application_created.png)
