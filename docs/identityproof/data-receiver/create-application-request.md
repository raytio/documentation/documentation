---
id: create-application-request
sidebar_label: Create AA Request
layout: article
title: Create Access Application Request
categories: [data-receiver]
featured: false
popular: false
tags: [data receiver, application, request]
---

:::note

For another way to create an AA request, visit [Application Request](/docs/identityproof/data-receiver/application-request)

:::note

Access application requests are used to request data from someone and to track whether theat person has completed the request.

1. To create a request via email, press the **Share** button next to the link you would like to create a request for, then email.
   ![image of an access application](/img/images/raytio/data-receiver/view_aa.png)
2. Enter the email address(es) of the people that you wish to send the request to
   ![image of the access application dropdown](/img/images/raytio/data-receiver/request_email.png)
3. Each recipient will receive an email that looks something like this:
   ![image of the send out form via email page](/img/images/raytio/data-receiver/request_email_received.png)
4. The user can then click on the link to provide the requested information to you

:::note

You may also share an access application via a link, embedded form or with a qr code. For these options, under the **Share** dropdown next to the desired link press on the favoured option and follow the steps

:::note
