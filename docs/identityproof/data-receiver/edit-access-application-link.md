---
id: edit-access-application-link
sidebar_label: Edit Existing AA Link
layout: article
title: Edit Existing Access Application Link
categories: [data-receiver]
featured: false
popular: false
tags: [data receiver, application, link]
---

You can make changes to existing access application links as neccessary.

1. Navigate to the **Applications** homepage and select the access application you would like to edit  
   ![image of the applications home page](/img/images/raytio/data-receiver/accounts_homepage.png)
2. At the bottom of the page there should be a list of links underneath the heading **Application Links**  
   ![image of the application links](/img/images/raytio/data-receiver/application_links.png)
3. To make changes to a link, click **Manage** and then **Edit**. If prompted, enter your passcode and press **Authorize**  
   ![image of the manage edit menu](/img/images/raytio/data-receiver/manage_edit.png)
4. You will now be able to make changes to your access application link by following the creation process
