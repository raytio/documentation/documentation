---
id: identity-check
sidebar_label: Identity Check
layout: article
title: Identity Check
categories: [data-receiver]
featured: false
popular: false
tags: [data receiver, identity]
---

Instead of sending someone an access application link to collect their data, you can manually fill out the form on their behalf

1. To perform an identity check, navigate to the Raytio homepage. In the top left corner of your screen select the **Data Receiver** dropdown box, and then select **Identity Check**

   ![image of the data receiver dropdown box](/img/images/raytio/data-receiver/identity_check.png)

2. Select the form that you would like to fill out  
   ![image of the identity check homepage](/img/images/raytio/data-receiver/identity_check_homepage.png)
3. Fill out the form, and then select **Submit**. The entered information will then appear in your submissions list as if it had been completed by the user  
   ![image of an access application form filled out](/img/images/raytio/data-receiver/identity_check_form.png)
