---
id: manage-access-application-permissions
sidebar_label: Manage AA Permissions
layout: article
title: Manage Access Application Permissions
categories: [data-receiver]
featured: false
popular: false
tags: [data receiver, application, permissions]
---

Only permitted users may edit an Access Application and view the associated submissions. Organization users must be specifically granted permission to each Access Application.

1. Ensure that the user you wish to authorize is a [member of your organization](../organization/invite-users-to-org.md).
2. Ensure you are viewing your organization account by toggling the account switcher ![Image of the mode switch icon](/img/images/raytio/account/mode_switch_icon.png) in the menu bar.
3. Navigate to the Applications menu, expand the appliction that you wish to grant permissions to and select the **Manage** button, then select **Manage who can edit this form**.
4. Select the user(s) who will be granted permission to this Access Application. If the user is not listed then they are not currently a [member of your organization](../organization/invite-users-to-org.md) and must accept the invitation to [join the organization](../organization/join-org.md).
5. Select the appropriate permission:

| Permission | Description                                                              |
| ---------- | ------------------------------------------------------------------------ |
| Views      | Can view submissions                                                     |
| Edits      | Can view submissions and edit the Access Application                     |
| Admins     | Can view submissions, edit the Access Application and manage permissions |

6. Click **Share**. Once the sharing process has completed a message will be displayed **Your access application was successfully shared**.
