---
id: share-access-application
sidebar_label: AA Share
layout: article
title: Share Access Applications
categories: [data-receiver]
featured: false
popular: false
tags: [data receiver, share, link, iframe, embed, qr code, email]
---

You can share an access application link with someone else in multiple ways:

- Link - copy the link into your own email, CRM or marketing system.
- Email - send an email directly from the Raytio application to the person that you want to collect information from. This will create an [application request](create-application-request) that you can track through the app to make sure that the person completes the form
- Embed - use the code to embed an Iframe without your web page. This allows the user to fill in the form without leaving your web site.
- QR Code - include the QR code on a printed poster or in an email so that mobile or tablet users can easily load the form on their device

1. Navigate to the desired application, and link  
   ![image of the chosen access application](/img/images/raytio/data-receiver/access-application.png)

2. At the bottom of the application, press on the **Share** button for the desired link you would like to share for that particular application.  
   ![image of the options for sharing an aa link](/img/images/raytio/data-receiver/share_aa.png)

   **Select the method of sharing that you would like to use**

3. Pressing **Copy Link** will copy the link to your clipboard, so you can paste it, and share it with anyone for them to fill out your form.

4. Pressing **Email** will bring up a form, enter the email of the person(s) you would like to send the link to, you may also add a reference to help identify individual recipients  
   ![image of the share via email popup](/img/images/raytio/data-receiver/share_email.png)

5. Pressing **Embed** will show you an embed link that can be put in a website, and link straight to your form.  
   ![image of the embed link](/img/images/raytio/data-receiver/embed.png)

6. Pressing **QR Code** will create a code that can be downloaded, so it can be shared, and scanned by people going to fill out your form  
   ![image of the qr code](/img/images/raytio/data-receiver/qrcode.png)
