---
id: add-subscription
sidebar_label: Subscriptions
layout: article
title: Manage Subscriptions
categories: [account-management]
featured: false
popular: false
tags: [org, subscription]
---

1. To add a subscription to your Raytio profile, navigate to the **Organizations** page, then click **Manage** > **Manage Subscriptions**
   ![image of the manage organization menu](/img/images/raytio/account/org_manage.png)
2. Choose the subscription plan you would like, and then scroll down to the bottom of the page and click **Buy Subscription**
   ![image of the raytio subscriptions page](/img/images/raytio/account/buy_subscription.png)
3. To add a card press the **Add a New Card** button
   ![image of the raytio subscriptions page](/img/images/raytio/account/review_purchase_no_card.png)
4. Enter the details of your credit card, and then click **Add**  
   ![image of the raytio subscriptions page](/img/images/raytio/account/add_card.png)
5. Once your card has been added, tick the card, and click **Submit Payment**
   ![image of the raytio subscriptions page](/img/images/raytio/account/add_subscription.png)
6. You should be redirected to a page with all your current subscriptions on it, including the one you just added
   ![image of the raytio subscriptions page](/img/images/raytio/account/subscriptions_page.png)
