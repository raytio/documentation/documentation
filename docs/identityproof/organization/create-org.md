---
id: create-organization
sidebar_label: Create Org
layout: article
title: Create Organization
categories: [account-management]
featured: false
popular: false
tags: [org, create]
---

An organization is a billing entity that has associated service subscriptions and payment details. An organization can have a list of authorized users that have accepted an invitation to join the organization. Organization users can then be added to the list of authorized users for an Access Application.

1. To create a Raytio organization, choose **My Account** > **Create an Organization**  
   ![Image of My Account menu wtih Create Organization](/img/images/raytio/account/my_account_org.png)
2. Fill out the form with your information, and then click **Submit**
   ![Image of organization create page filled out](/img/images/raytio/account/create_org_form.png)
3. You should be redirected to a screen that looks like this: _You have successfully created an organization_
   ![Image of organization page](/img/images/raytio/account/my_organization_created.png)
