---
id: invite-users-to-organization
sidebar_label: Invite Users
layout: article
title: Invite Users to Organization
categories: [account-management]
featured: false
popular: false
tags: [org, invite]
---

1. To invite users to your organization, ensure you are viewing your organization account by toggling the account switcher ![Image of the mode switch icon](/img/images/raytio/account/mode_switch_icon.png) in the menu bar.
2. Select the **Invite Someone** button at the bottom of the Raytio Dashboard.  
   ![image of the invite someone to join organization page](/img/images/raytio/account/invite_to_org.png)
3. Enter the email of the person you would like to invite to your organization.  
   ![image of the invite someone to join organization form page](/img/images/raytio/account/invite_to_org_form.png)
4. Then press the **Submit** button. The person will be sent an email inviting them to join your organization.
