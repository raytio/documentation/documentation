---
id: join-organization
sidebar_label: Join Org
layout: article
title: Join Organization
categories: [account-management]
featured: false
popular: false
tags: [org, join]
---

:::info

Before you join an organization, make sure you have created and signed into a Raytio account matching the email that the invite was sent to

:::info

1. To join an organization, first confirm that the owner of the organization has invited you to join. Then navigate to your email client and locate an email from **hello@rayt.io** with a subject of "Raytio join organisation request":
   ![image of the raytio join organization email](/img/images/raytio/account/join_org_email.png)
2. Click on the link and you will be redirected to the home page of the organization, with a notification at the top that says **You've successfully joined the organization!**  
   ![image of the raytio joined org page](/img/images/raytio/account/success_joined_org.png)
