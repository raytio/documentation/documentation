---
id: organizations
sidebar_label: Change Organization
layout: article
title: Switching Organizations
categories: [account-management]
featured: false
popular: false
tags: [org]
---

There are two application modes:

- personal mode: use this to manage your personal data. In this mode you are storing and sharing data with another party
- organization mode: use this to manage your organization and Access Application. In this mode you are requesting, receiviing and viewing data shared by other parties

The application has two different menu bars depending on the mode:

1. Personal menu bar ![Image of the personal menu](/img/images/raytio/account/menu_bar_personal.png)
2. Organization menu bar ![Image of the organization menu](/img/images/raytio/account/menu_bar_org.png)

You can switch modes by clicking on the switch icon ![Image of the mode switch icon](/img/images/raytio/account/mode_switch_icon.png) in the menu bar.
:::info

The switch icon only appears if you have already created an organization

:::info

If you have created or belong to multiple organizations then you will have a list of options available on the switch menu:
![Image of the mode switch icon with multiple options](/img/images/raytio/account/mode_switch_icon_multi.png)

Choose "Personal Account" to switch to personal mode or choose one of the listed organizations to maintain it.
