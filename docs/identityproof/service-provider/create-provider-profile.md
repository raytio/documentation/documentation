---
id: create-provider-profile
sidebar_label: Create Profile
layout: article
title: Create Provider Profile
categories: [service-provider]
featured: false
popular: false
tags: [service provider, profile]
---

A service provider is (typically) an organisation that provides services to the public.

1. To create a provider profile, press on the **Service Provider** dropdown at the top of your screen, then on **Provider Profile** you should be taken to the Service Provider page
   ![image of the service provider profile page](/img/images/raytio/service-provider/service-provider.png)
2. At the bottom of the page, press **Create New Provider Profile**, you will then be taken to this page:
   ![image of the create provider profile page](/img/images/raytio/service-provider/create-provider-profile.png)
3. Complete the form (You can upload an image from your computer), and press submit to create your provider profile  
   ![image of the create provider profile form filled out](/img/images/raytio/service-provider/provider-profile-form.png)
4. You should then be taken to a page that shows your newly created service provider
   ![image of the newly created provider profile](/img/images/raytio/service-provider/created-service-provider.png)
