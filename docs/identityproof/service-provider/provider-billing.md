---
id: provider-billing
sidebar_label: Setup Billing
layout: article
title: Setup Provider Billing
categories: [service-provider]
featured: false
popular: false
tags: [service provider, profile, billing]
---

:::important

Refer [Online check-in setup](/tutorials/online-checkin-setup.md) to get your Stripe publishable and restricted API keys

:::important

1. In the Service Provider dropdown, press on **Provider Billing**, and press **Create New Provider Billing**, to start the process  
   ![image of the provider billing create new page](/img/images/raytio/service-provider/provider-billing.png)

2. Press **Create New Provider Billing**, you will be taken to the provider billing form, select **Stripe** as your billing provider, and enter your publishable API key, then press **Submit**
3. On the next page, select the billing provider that you just created, then under Billing Key,press the **+**, then press **Create New**, and then enter your restricted API key.
   ![gif of the steps above being executed](/img/images/raytio/service-provider/add-billing-key.gif)
4. Finally, press **Next**, and then you should be taken to your newly created billing profile with the new billing key.
   ![image of the provider billing screen, with the new billing key created](/img/images/raytio/service-provider/billing-key.png)
