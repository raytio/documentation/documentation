---
id: provider-prices
sidebar_label: Provider Prices
layout: article
title: Provider Prices
categories: [service-provider]
featured: false
popular: false
tags: [service provider, profile, billing]
---

1. To create provider prices, press on the **Service Provider** dropdown at the top of your screen, then on **Provider Prices** you should be taken to the Provider Prices page
   ![image of the provider prices homepage](/img/images/raytio/service-provider/provider_prices.png)
2. From the dropdown in the top right that by default displays **All Public Providers** choose your desired provider billing profile, and then click on the orange **+**.

   ![image of the add price page](/img/images/raytio/service-provider/add_price.png)

3. You will then be taken to the add price form, once completed press **Submit**
   ![image of the add price form completed](/img/images/raytio/service-provider/add_price_completed.png)
4. You will then be taken back to the Provider Prices pages, where you will see your newly created price
   ![image of the provider prices homepage with newly created price](/img/images/raytio/service-provider/provider_price_created.png)
