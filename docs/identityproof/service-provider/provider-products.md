---
id: provider-products
sidebar_label: Provider Products
layout: article
title: Provider Products
categories: [service-provider]
featured: false
popular: false
tags: [service provider, profile, billing]
---

1. To create provider products, press on the **Service Provider** dropdown at the top of your screen, then on **Provider Products** you should be taken to the Provider Products page
   ![image of the provider products homepage](/img/images/raytio/service-provider/provider_products.png)
2. From the dropdown in the top right that by default displays **All Public Providers** choose your desired provider billing profile, and then click on the orange **+**.

   ![image of the add product page](/img/images/raytio/service-provider/add_product.png)

3. You will then be taken to the add product form, once completed press **Submit**
   ![image of the add product form completed](/img/images/raytio/service-provider/add_product_completed.png)
4. You will then be taken back to the Provider Product pages, where you will see your newly created product
   ![image of the provider product homepage with newly created product](/img/images/raytio/service-provider/provider_product_created.png)
