---
id: sign-pdf
sidebar_label: Sign PDF
layout: article
title: Sign PDF
categories: [tools]
featured: false
popular: false
tags: [tools, sign]
---

You can use Raytio to add a signature to a PDF document

1. Navigate to the **Tools** dropdown box in the top right corner of your screen, then select **Sign PDF**

   ![ tools dropdown](/img/images/raytio/tools/sign_pdf.png)

2. The **Sign PDF** page will be displayed. **Step 1 of 4** is to upload the document to be signed. To start the signature process, press **Upload**, and then select the pdf you would like to add the signature to, and press **Open**
   ![ sign pdf homepage](/img/images/raytio/tools/sign_pdf_homepage.png)
3. **Step 2 of 4** is to either draw your signature in the box provided, or select **Upload a Photo** to select and upload an image of your signature. Then select **Next**  
   ![ upload or create signature page](/img/images/raytio/tools/signature.png)
4. **Step 3 of 4** allows you to add the signature to the file and add text such as a name and date. Navigate to the page where you want to place the text or signature by selecting the page number  
   ![sign pdf page navigation](/img/images/raytio/tools/document_navigation.png)
5. To add a signature, select **Add Signature**. To add text, select **Add Text** and enter the text that you wish to add. The signature or text will be placed in the top left corner of the page  
   ![add signature and add text buttons](/img/images/raytio/tools/add_signature_text_buttons.png)
6. To position the item in the appropriate place in the document, click on the item and a box will appear around it. Then click inside the box and drag it to the appropriate place. Press and hold on the square in the bottom right corner of the image to resize the image.  
   ![animated gif showing moving and resizing a signature](/img/images/raytio/tools/place_resize_signature.gif)
7. Once you have finished placing the text and this signature on all of the pages, press **Finish**  
   ![finish button](/img/images/raytio/tools/finish_button.png)
8. **Step 4 of 4**. If you would like to repeat this process to add another signature, press **Add Another Signature**. If you are finished and would like to download the file, press **Download**  
   ![ sign pdf completed page](/img/images/raytio/tools/download_signature.png)
