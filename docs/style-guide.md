---
id: conventions
title: Documentation conventions
sidebar_label: Conventions
---

This pages describes the various conventions used throughout our documentation.

## Headers

# H1 - Create the best documentation

## H2 - Create the best documentation

### H3 - Create the best documentation

#### H4 - Create the best documentation

##### H5 - Create the best documentation

###### H6 - Create the best documentation

---

## Emphasis

_Emphasis_, aka _italics_.

**Strong emphasis**, aka **bold**.

**_Combined emphasis_**.

~~Strikethrough~~

---

## Lists

Lists may be ordered:

1. First ordered list item
1. Another item  
   Unordered sub-list  
   And again
1. Actual numbers don't matter, just that it's a number
   1. Ordered sub-list
   2. Next entry
      1. Next level
1. And another item.

Lists may also be unordered:

- Unordered list are indicated by a bullet point
- i.e. no numbering
  1. Order sub-lists may be used also

---

A horizontal line indicates a section break.

---

## Code

Code will appear in highlighted blocks.

```javascript
var s = "JavaScript syntax highlighting";
alert(s);
```

```python
s = "Python syntax highlighting"
print(s)
```

```
No language indicated, so no syntax highlighting.
But let's throw in a <b>tag</b>.
```

```js {2}
function highlightMe() {
  console.log("This line can be highlighted!");
}
```

---

## Tables

Tables are used to lay out data.

| Tables        |      Are      |   Cool |
| ------------- | :-----------: | -----: |
| col 3 is      | right-aligned | \$1600 |
| col 2 is      |   centered    |   \$12 |
| zebra stripes |  on each row  |    \$1 |

---

## Blockquotes

> Blockquotes are used to clarify specific points or to quote third parties.

---

## Admonitions

:::note

This is a note

:::

:::tip

This is a tip

:::

:::important

This is important

:::

:::caution

This is a caution

:::

:::warning

This is a warning

:::
