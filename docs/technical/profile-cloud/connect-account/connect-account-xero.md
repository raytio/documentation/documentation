---
id: connect-xero
title: Connect Xero account
sidebar_label: Connect Xero
---

## Connect Xero account process flow

![](/img/diagrams/connect_account_xero.svg)
