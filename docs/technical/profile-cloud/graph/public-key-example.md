Public key example

```json
{
  "n_id": "a40a13a0-d8fd-472c-9c69-0405faa12b0f",
  "properties": {
    "alg": "RSA-OAEP-512",
    "n": "7v7R8pAe65QiZEdG2pZyt5FLhWpLWIjBoXd8l3oY5mY1nkAdfEieK0p-3NbqZv5cUn8zqjLXkBttUDseyxtDxnj0gFfSLZ8voDw-BHvSiaXDGNv0wpgbVJVzJF6y0EpBap1q30bf-Ohs3Ss9FhMT41n062zL4A9MQWczwcKsAeZaqGP7cEUpFErtyTmUjAtYc9949BlsBgQqlR9Qc8sfTfy04CjHVY6FdJcdO3TqJcSNMBMzljOLkJdqV0F_ZZjTBzd1CXgzTsYZcbY-J5GrJUqiT-KmslJZLWPGiFof5nieFeNr98HPDK9DmaAP8RVqOfEtPZnR3iLtU9j-cwBoRw",
    "kty": "RSA",
    "e": "AQAB",
    "ext": true,
    "key_ops": ["wrapKey"],
    "n_id": "a40a13a0-d8fd-472c-9c69-0405faa12b0f"
  },
  "labels": ["PublicKey"]
}
```
