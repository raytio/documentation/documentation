---
id: create-payment-method
title: Create payment method
sidebar_label: Create payment
---

## Create payment method process flow

![](/img/diagrams/create_payment_method.svg)
