---
id: create-subscription
title: Create subscription
sidebar_label: Create subscription
---

## Create subscription process flow

![](/img/diagrams/create_subscription.svg)
