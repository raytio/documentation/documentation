---
id: how-it-works-business
sidebar_label: How it works
layout: article
title: How it works
categories: [tutorial]
featured: false
popular: false
tags: [business, how it works]
---

1. Email a user a link, print out a QR code, or embed the form into your web site.  
   ![image of the raytio application link share dialog](/img/images/raytio/tutorials/AA_link_share.png)
2. Your customer follows the link on their web browser, phone or tablet and is directed to a web page with your branding where they sign up or sign in  
   ![image of the branded sign in page](/img/images/raytio/tutorials/branded_signin_page.png)
3. The user uploads their identity documents and enters other information as requested by you. Live person detection, and biometric checks are included  
   ![image of scan driver licence page](/img/images/raytio/tutorials/scan_dl_front.png)
4. The forms are completely customisable so you can collect whatever information is required for your business process  
   ![image of the raytio subscriptions page](/img/images/raytio/tutorials/custom_form.png)
5. Raytio automatically verifies all the information that we can with trusted third parties  
   ![image of the company data verifications](/img/images/raytio/tutorials/company_details_verified.png)
6. Keep track of requests that you've sent and re-send requests if necessary  
   ![image of the raytio application requests page](/img/images/raytio/tutorials/AA_requests_view.png)
7. Once the user submits the information you receive a notification by email or webhook. You can then view and print the shared data and verifications  
   ![image of the view instance data page](/img/images/raytio/tutorials/view_shared_data.png)
