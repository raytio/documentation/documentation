---
id: online-checkin-setup
sidebar_label: Online check-in
layout: article
title: Set up online check-in
categories: [tutorial]
featured: false
popular: false
tags: [business, accommodation, check-in]
---

## Introduction

Raytio online check-in enables accommodation providers to offer guests a seamless a nd customisable check-in process that provides assurance to the provider that the guest is who they say they are.

## Setup wizard

The simplest way to get started with online check-in is to use the onboarding wizard which will automatically set up all of the necessary system elements that will meet the needs of most accommodation providers. Alternatively you can set up Raytio online check-in manually.

## Step 1. Set up your service provider

Set your organization up in Raytio as a service provider. This enables your products and services to be listed in the marketplace. It is the entity to which your name, logo and billing provider information is attached.

## Step 2. Obtain your Stripe API keys

Follow the steps below to obtain your Stripe publishable key and create a restricted key.

1. Login to [Stripe](https://dashboard.stripe.com) and navigate to Developers > [API keys](https://dashboard.stripe.com/apikeys).
2. Copy the value in the `Token` column for the `Publishable key`
3. Select [Create restricted key](https://dashboard.stripe.com/apikeys/create). Enter a key name.
4. Set the following permissions

   | Resource       | Permission |
   | -------------- | ---------- |
   | Customers      | Write      |
   | PaymentIntents | Write      |
   | SetupIntents   | Write      |

5. Select "Create key"
6. Copy the value in the `Token` column for the restricted key that you just created

## Step 3. Set up your billing provider

Your billing provider details are used to securely store your customer's payment information including credit card, customer and payment details. These details are stored directly with your billing provider - Raytio does not store or have access to your customer's credit card information.

Raytio currently supports Stripe as a billing provider.

## Step 4. Create your Raytio billing account (Organization)

A Raytio organization is a billing entity that has associated service subscriptions and payment details. In order to provide the online check-in service to your guests you need to create a billing account and purchase a suitable subscription:

1. [Create an organization](/identityproof/organization/create-org.md)
2. [Create a subscription](/identityproof/organization/add-subscription.md)

## Step 5. Create an Access Application

An Access Application ("AA") allows a group of users in a Raytio organization to receive and view data by Raytio users (data providers). Before commencing setup up the AA, decide:

1. The name of the AA - this will be visible to users on email invites and on the form
2. Logo -
3. What information to collect. The list of schema that are associated with an AA defines at a high level the types of information to collect
4. If you need to collect any custom information that is not already defined by Raytio then you will neded to create a custom schema.

Once you have the necessary information, create the Access Application.

## Step 6. Create the AA forms

The Raytio form wizard enables a series of forms to be defined that describe the type of information to be collected from users and what data will be shared with the data receiver. For each
