---
id: welcome
title: Welcome
sidebar_label: Welcome
---

Welcome to the Raytio documentation.

Refer to the menu on left to navigate around the documentation:

- Features outline the capabilities and functionalities of the systems
- User Guides describe each system function
- Tutorials provides step-by-step guides on how to use our products, typically for a specific process
- Technical Reference provide reference information about the system architecture
- Knowledge Base contains background information, regulatory details and terminology
