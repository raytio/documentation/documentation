// @ts-check
module.exports = /** @type {import("@docusaurus/types").DocusaurusConfig} */ ({
  title: "Raytio",
  tagline: "Online Identity Verification",
  url: "https://docs.rayt.io",
  baseUrl: "/",
  favicon: "img/favicon.ico",
  organizationName: "raytio", // Usually your GitHub org/user name.
  projectName: "Raytio website", // Usually your repo name.
  scripts: ["https://d3dfiqfbrgk5v0.cloudfront.net/hello.js"],
  themeConfig: {
    navbar: {
      title: "Raytio",
      logo: {
        alt: "Raytio Logo",
        src: "img/logo.svg",
      },
      items: [
        // {
        //   to: "/",
        //   activeBasePath: "/index",
        //   label: "Business",
        //   position: "right",
        // },
        // {
        //   to: "features",
        //   activeBasePath: "features",
        //   label: "Features",
        //   position: "right",
        // },
        {
          to: "/docs/welcome",
          activeBasePath: "docs/welcome",
          label: "Documentation",
          position: "left",
        },
        {
          to: "https://api-docs.rayt.io",
          label: "Developers",
          position: "left",
        },
        {
          to: "https://app.rayt.io/signup",
          label: "Try it for Free",
          position: "right",
        },
        {
          to: "https://app.rayt.io/login",
          label: "Log in",
          position: "right",
        },
      ],
    },
    footer: {
      style: "dark",
      links: [
        {
          title: "Help",
          items: [
            {
              label: "IdentityProof User Guide",
              to: "docs/identityproof/account/create-an-account",
            },
            {
              label: "SecretSafe User Guide",
              to: "docs/article/features/folders",
            },
          ],
        },
        {
          title: "Legal",
          items: [
            {
              label: "Privacy",
              to: "/privacy",
              activeBasePath: "privacy",
            },
            {
              label: "Licence",
              to: "/mit",
              activeBasePath: "mit",
            },
          ],
        },
        {
          title: "About",
          items: [
            {
              label: "Contact",
              to: "contact",
            },
          ],
        },
        {
          title: "Social",
          items: [
            {
              label: "Blog",
              to: "blog",
            },
            {
              label: "GitLab",
              href: "https://gitlab.com/raytio",
            },
            {
              label: "Twitter",
              href: "https://twitter.com/raytiohq",
            },
            {
              label: "LinkedIn",
              href: "https://www.linkedin.com/company/raytio",
            },
          ],
        },
      ],
      copyright: `Copyright © ${new Date().getFullYear()} Raytio Ltd.`,
    },
    typesense: {
      typesenseCollectionName: "raytio-docs", // Replace with your own doc site's name. Should match the collection name in the scraper settings.

      typesenseServerConfig: {
        nodes: [
          {
            host: "3.231.107.207",
            port: 8108,
            protocol: "http",
          },
        ],
        apiKey: "KAXbJonXaNr2jQZPGNAaeQ6n1qPcVQM2",
      },

      // Optional: Typesense search parameters: https://typesense.org/docs/0.21.0/api/search.md#search-parameters
      typesenseSearchParameters: {},

      // Optional
      contextualSearch: true,
    },
  },
  themes: ["docusaurus-theme-search-typesense"],
  presets: [
    [
      "@docusaurus/preset-classic",
      {
        docs: {
          sidebarPath: require.resolve("./sidebars.js"),
          editUrl:
            "https://gitlab.com/raytio/documentation/documentation/-/edit/master",
        },
        theme: {
          customCss: require.resolve("./src/css/custom.css"),
        },
      },
    ],
  ],
});
