module.exports = {
  someSidebar: [
    {
      type: "category",
      label: "Getting Started",
      collapsible: true,
      collapsed: true,
      link: {
        type: "doc",
        id: "welcome",
      },
      items: ["conventions"],
    },
    {
      type: "category",
      label: "Features",
      collapsible: true,
      collapsed: true,
      link: {
        type: "generated-index",
      },
      items: [
        {
          type: "category",
          label: "Identity Proof",
          collapsible: true,
          collapsed: true,
          link: {
            type: "doc",
            id: "features/features-introduction",
          },
          items: [
            "features/data-extraction",
            "features/data-verification",
            "features/pep-checks",
            "features/biometric-checks",
            "features/user-portal",
            "features/form-features",
          ],
        },
        {
          type: "doc",
          label: "Secretsafe",
          id: "features/introduction-secretsafe",
        },
      ],
    },
    {
      type: "category",
      label: "User Guides",
      collapsible: true,
      collapsed: true,
      link: {
        type: "generated-index",
      },
      items: [
        {
          type: "category",
          label: "Identity Proof",
          collapsible: true,
          collapsed: true,
          link: {
            type: "generated-index",
          },
          items: [
            {
              Account: [
                "identityproof/account/personally-identifying-email",
                "identityproof/account/create-an-account",
                "identityproof/account/settings",
                "identityproof/account/change-password",
                "identityproof/account/two-factor-auth",
                "identityproof/account/hardware-auth",
                "identityproof/account/export-keys",
                "identityproof/account/import-keys",
                "identityproof/account/audit-trail",
                "identityproof/account/state-transitions",
              ],
            },
            {
              Organization: [
                "identityproof/organization/create-organization",
                "identityproof/organization/invite-users-to-organization",
                "identityproof/organization/join-organization",
                "identityproof/organization/organizations",
                "identityproof/organization/add-subscription",
              ],
            },
            {
              "Data Provider": [
                "identityproof/data-provider/profile-objects",
                "identityproof/data-provider/file-manager",
                "identityproof/data-provider/shared-data",
                "identityproof/data-provider/reshare-data",
                "identityproof/data-provider/verifications",
                "identityproof/data-provider/share-verifications",
                "identityproof/data-provider/profile-permissions",
                "identityproof/data-provider/file-organization",
                "identityproof/data-provider/todo",
              ],
            },
            {
              "Data Receiver": [
                "identityproof/data-receiver/create-access-application",
                "identityproof/data-receiver/create-access-application-link",
                "identityproof/data-receiver/edit-access-application-link",
                "identityproof/data-receiver/create-application-request",
                "identityproof/data-receiver/share-access-application",
                "identityproof/data-receiver/submissions",
                "identityproof/data-receiver/application-actions",
                "identityproof/data-receiver/identity-check",
                "identityproof/data-receiver/application-request",
                "identityproof/data-receiver/manage-access-application-permissions",
              ],
            },
            {
              "Service Provider": [
                "identityproof/service-provider/create-provider-profile",
                "identityproof/service-provider/provider-billing",
                "identityproof/service-provider/provider-prices",
                "identityproof/service-provider/provider-products",
              ],
            },
            {
              Tools: ["identityproof/tools/sign-pdf"],
            },
          ],
        },
        {
          type: "category",
          label: "Secret Safe",
          collapsible: true,
          collapsed: true,
          link: {
            type: "generated-index",
          },
          items: [
            {
              Account: [
                "article/account/change-your-email",
                "article/account/change-your-master-password",
                "article/account/delete-your-account",
                "article/account/export-your-data",
                "article/account/forgot-master-password",
                "article/account/lost-two-step-device",
                "article/account/setup-two-step-login-authenticator",
                "article/account/setup-two-step-login-duo",
                "article/account/setup-two-step-login-email",
                "article/account/setup-two-step-login-u2f",
                "article/account/setup-two-step-login-yubikey",
                "article/account/setup-two-step-login",
                "article/account/update-encryption-key",
              ],
            },
            {
              "Directory Connector": [
                "article/directory-connector/azure-active-directory",
                "article/directory-connector/directory-sync",
                "article/directory-connector/gsuite-directory",
                "article/directory-connector/ldap-directory",
                "article/directory-connector/okta-directory",
                "article/directory-connector/onelogin-directory",
                "article/directory-connector/user-group-filters",
              ],
            },
            {
              Features: [
                "article/features/attachments",
                "article/features/authenticator-keys",
                "article/features/auto-fill-android",
                "article/features/auto-fill-browser",
                "article/features/custom-fields",
                "article/features/fingerprint-phrase",
                "article/features/folders",
                "article/features/searching-vault",
                "article/features/uri-match-detection",
              ],
            },
            {
              Importing: [
                "article/importing/import-data",
                "article/importing/import-from-1password",
                "article/importing/import-from-chrome",
                "article/importing/import-from-lastpass",
              ],
            },
            {
              Organizations: [
                "article/organizations/getting-started-organizations",
                "article/organizations/what-is-an-organization",
                "article/organizations/collections",
                "article/organizations/groups",
                "article/organizations/managing-users",
                "article/organizations/policies",
                "article/organizations/public-api",
              ],
            },
            {
              Security: [
                "article/security/can-secretsafe-see-my-passwords",
                "article/security/cloud-server-security",
                "article/security/how-is-data-securely-transmitted-and-stored",
                "article/security/is-secretsafe-audited",
                "article/security/password-salt-hash",
                "article/security/what-encryption-is-used",
                "article/security/what-happens-if-secretsafe-is-hacked",
                "article/security/what-information-is-encrypted",
                "article/security/where-is-data-stored-cloud",
                "article/security/where-is-data-stored-computer",
                "article/security/why-should-i-trust-secretsafe",
              ],
            },
            {
              Miscellaneous: [
                "article/miscellaneous/blocker-access-rule",
                "article/miscellaneous/cli",
                "article/miscellaneous/extension-wont-load-in-private-mode",
                "article/miscellaneous/localization",
                "article/miscellaneous/website-icons",
              ],
            },
          ],
        },
      ],
    },
    {
      Tutorials: [
        {
          Business: [
            "tutorials/how-it-works-business",
            "tutorials/online-checkin-setup",
          ],
        },
      ],
      "Technical Reference": [
        {
          "Identity Proof": [
            {
              Authenticate: [
                "technical/profile-cloud/authenticate/sign-up",
                "technical/profile-cloud/authenticate/sign-in",
                "technical/profile-cloud/authenticate/sign-up-verification",
              ],
              Schema: [
                "technical/profile-cloud/schema/create-schema",
                "technical/profile-cloud/schema/read-schema",
              ],
              "Profile Object": [
                "technical/profile-cloud/profile-object/create-po",
                "technical/profile-cloud/profile-object/read-po",
              ],
              "Extract, Sign and Verify": [
                "technical/profile-cloud/extract-verify/extract-data",
                "technical/profile-cloud/extract-verify/verify-data",
                "technical/profile-cloud/extract-verify/verify-signature",
              ],
              Organization: [
                "technical/profile-cloud/organization/create-payment-method",
                "technical/profile-cloud/organization/create-subscription",
              ],
              Share: [
                "technical/profile-cloud/share/create-aa",
                "technical/profile-cloud/share/share-data",
                "technical/profile-cloud/share/read-shared-data",
              ],
              "Connect Account": [
                "technical/profile-cloud/connect-account/connect-xero",
              ],
            },
          ],
        },
      ],
      "Knowledge Base": [{ "NZ Compliance": ["compliance/nz/nz-cdd"] }],
      "Assets, Files, Documents": ["assets/forms"],
    },
  ],
};
