import React from "react";
import classnames from "classnames";
import Layout from "@theme/Layout";
import styles from "./styles.module.css";

const browsersWeSupport = [
  ["Chrome", "https://google.com/chrome"],
  ["Firefox", "https://mozilla.org/firefox/new"],
  ["Safari"],
  ["Microsoft Edge", "https://microsoft.com/edge"],
];

function Home() {
  return (
    <Layout
      title="Browser not supported"
      description="Unfortunately your web browser does not support the latest security features Raytio requires to ensure your privacy and the security of your data."
    >
      <header className={classnames("hero hero--primary", styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">Your browser is not supported</h1>
        </div>
      </header>
      <main>
        <section className={styles.features}>
          <div className="container">
            <p>
              Unfortunately your web browser does not support the latest
              security features Raytio requires to ensure your privacy and the
              security of your data.
              <br />
              <br />
              Our supported browsers are:
              <ul>
                {browsersWeSupport.map(([name, url]) => (
                  <li key={name}>
                    {url ? (
                      <a href={url} rel="noopener noreferrer" target="_blank">
                        {name}
                      </a>
                    ) : (
                      name
                    )}
                  </li>
                ))}
              </ul>
              <a href="https://rayt.io/privacy">
                If you'd like to learn more about the data security model used
                by Raytio, click here
              </a>
            </p>
          </div>
        </section>
      </main>
    </Layout>
  );
}

export default Home;
