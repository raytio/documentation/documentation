import React from "react";
import classnames from "classnames";
import Layout from "@theme/Layout";
import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import useBaseUrl from "@docusaurus/useBaseUrl";
import styles from "./styles.module.css";

const features = [
  {
    title: <>regulatory compliance</>,
    description: (
      <>
        Performing Anti-Money Laundering (AML) checks and Customer Due Diligence
        (CDD) of legal entities such as companies and trusts can be time
        consuming and painful. Raytio is an online identity verification
        solution making it easy for Law Firms, Accounting Practices and Real
        Estate Agencies to comply with Anti-Money Laundering (AML) legislation.
      </>
    ),
  },
  {
    title: <>verified information</>,
    description: (
      <>
        We verify your customer’s information with trusted, authoritative
        sources; this includes the DIA for passport, NZTA for driver license,
        Police, and companies information. We also check with banks for source
        transaction data, accounting system providers such as Xero, utility and
        telecom providers plus international PEP and sanction watchlists. An
        audit trail of all checks is available.
      </>
    ),
  },
  {
    title: <>Comprehensive data</>,
    description: (
      <>
        Clients share personal information with Raytio including identity
        information, bank transactions, business accounting records, income,
        expenses, credit facilities, investments and other assets, utility,
        telecom and insurance details, health records, qualifications and
        certifications.
      </>
    ),
  },
];

function Feature({ imageUrl, title, description }) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames("col col--4", styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Features() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  return (
    <Layout
      title="Online Identity Verification"
      description="Receive verified information securely from customers<head />"
    >
      <header className={classnames("hero hero--primary", styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">Easy AML Identity Verification</h1>
          <div className={styles.buttons}>
            <Link
              className={classnames(
                "button button--outline button--secondary button--lg",
                styles.getStarted
              )}
              to={useBaseUrl("https://meetings.hubspot.com/cameron-beattie")}
            >
              Book a Demo
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Features;
