import React from "react";
import classnames from "classnames";
import Layout from "@theme/Layout";
import Link from "@docusaurus/Link";
import useDocusaurusContext from "@docusaurus/useDocusaurusContext";
import useBaseUrl from "@docusaurus/useBaseUrl";
import styles from "./styles.module.css";

const features = [
  {
    title: <>SEAMLESS ONLINE AML & CDD SOLUTION</>,
    description: (
      <>
        Raytio’s seamless identity verification solution accelerates and
        automates the manual, repetitive compliance tasks that Accountants,
        Lawyers and Real Estate Agents must complete as part of Customer Due
        Diligence (CDD), Know Your Customer (KYC), and Anti-Money Laundering
        (AML) checks.
        <p>
          Raytio makes new customer onboarding easy by verifying your customers’
          identities with the right authorities, and keeping information
          current, so audit time is stress-free.
        </p>{" "}
      </>
    ),
  },
  {
    title: <>how can raytio help your business?</>,
    description: (
      <>
        Raytio helps you complete AML checks and streamline your KYC processes,
        but there are additional benefits too:
        <ul>Speed up client onboarding time</ul>
        <ul>Improve data accuracy</ul>
        <ul>Quicker, up-to-date results</ul>
        <ul>Make audit time faster & easier</ul>
        <ul>Reduce time & inconvenience for the customer</ul>
        <ul>Access to local Support team 24/7</ul>{" "}
      </>
    ),
  },
  {
    title: <>REUSABLE IDENTITY PROFILES Update automatically</>,
    description: (
      <>
        Raytio enhances the customer experience by creating an online, reusable
        identity profile for each client that can be shared as needed. This
        removes the inconvenience of filling in multiple forms that ask the same
        questions, over and over again. Client profiles update automatically as
        details change over time, so that you always have the most up-to-date
        information on hand. Now audit time is as easy as clicking a button.
      </>
    ),
  },

  {
    title: <>AML &amp; CDD Compliance</>,
    description: (
      <>
        Comply with anti-money laundering regulations by classifying,
        identifying, verifying and tracking potential customers. Reduce fraud by
        ensuring all required checks have been completed.
      </>
    ),
  },
  {
    title: <>Always Accurate</>,
    description: (
      <>
        Receive accurate verified data from customers directly to your system.
        Customer data updates are automatically notified so your information is
        always accurate.
      </>
    ),
  },
  {
    title: <>Encrypted and Secure</>,
    description: (
      <>
        Data privacy is assured because users are in control of their data.
        End-to-end encryption protects against fraud or unauthorized third party
        usage.
      </>
    ),
  },
];

function Feature({ imageUrl, title, description }) {
  const imgUrl = useBaseUrl(imageUrl);
  return (
    <div className={classnames("col col--4", styles.feature)}>
      {imgUrl && (
        <div className="text--center">
          <img className={styles.featureImage} src={imgUrl} alt={title} />
        </div>
      )}
      <h3>{title}</h3>
      <p>{description}</p>
    </div>
  );
}

function Home() {
  const context = useDocusaurusContext();
  const { siteConfig = {} } = context;
  return (
    <Layout
      title="Online Identity Verification"
      description="Share verified information securely with third parties.<head />"
    >
      <header className={classnames("hero hero--primary", styles.heroBanner)}>
        <div className="container">
          <h1 className="hero__title">
            Your Online solution for AML Identity Verification & KYC needs
          </h1>
          <div className={styles.buttons}>
            <Link
              className={classnames(
                "button button--outline button--secondary button--lg",
                styles.getStarted
              )}
              to={useBaseUrl("https://meetings.hubspot.com/cameron-beattie")}
            >
              Book a Demo
            </Link>
          </div>
        </div>
      </header>
      <main>
        {features && features.length && (
          <section className={styles.features}>
            <div className="container">
              <div className="row">
                {features.map((props, idx) => (
                  <Feature key={idx} {...props} />
                ))}
              </div>
            </div>
          </section>
        )}
      </main>
    </Layout>
  );
}

export default Home;
