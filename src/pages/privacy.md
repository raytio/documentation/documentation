---
title: Raytio Privacy Policy
author: Cameron Beattie
author_title: Founder and CEO
tags: [legal, privacy]
---

# Raytio Privacy Policy

We collect personal information from you, including information about your:

1. name, gender and date of birth;
1. contact information including physical address, email and phone details;
1. driver licence, passport, birth, and citizenship details;
1. computer or network that you use to access our services including location if you choose to share that with us;
1. interactions with us;
1. billing or purchase information; and
1. employment, income, expense, asset and liability information.

We collect your personal information in order to:

1. store that information securely on your behalf; and
1. verify your information with authoritative sources; and
1. (with your permission) allow you to share that verified information with third parties so they can confirm your identity when performing customer due diligence and complying with laws such as anti-money laundering regulations.

With your permission, we share this information with:

1. relevant third parties (such as government agencies), in order to validate your personal information such as name, date of birth, driver licence, passport, citizenship and address; and
1. specific third parties that you nominate to share the information with as part of their due dilgence and identification processes.

Providing some information is optional. If you choose not to enter personal information, we'll be unable to provide information verification or onboarding services.

We keep your information safe by storing the personal information that you provide to us in an encrypted format using keys that only you have access to. Our staff do not have access to your personally identifiable information. Third parties do not have access to your personally identifiable information unless you share your information with them.

You have the right to ask for a copy of any personal information we hold about you, and to ask for it to be corrected if you think it is wrong. If you’d like to ask for a copy of your information, or to have it corrected, please contact us at [hello@rayt.io](mailto:hello@rayt.io), or PO Box 32465, Devonport, Auckland 0744, New Zealand.

If you have any questions or concerns relating to our verification of your data with the Australian Government, please contact:  
Office of the Australian Information Commissioner  
GPO Box 5218, Sydney NSW 2001  
Telephone: 1300 363 992  
Fax: +61 2 9284 9666  
Email: [enquiries@oaic.gov.au](mailto:enquiries@oaic.gov.au)  
[View full details about making a complaint to the OAIC](https://www.oaic.gov.au/privacy/privacy-complaints/).
